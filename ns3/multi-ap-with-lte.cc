// 
// SCRIPT
//    multi-ap-with-lte.cc 
//
// DESCRIPTION
//   The default network topology includes 1 content server, one eNodeB, 3 UEs(
// also as wifi ap installed in the bus), and 1 wifi sta(as people, the user).
// The user stayed still on the side road,  and when a bus comes here, the 
// user will connect to the very first ap(installed on the bus, which firstly
// responds it via DHCP**AND HERE IS WHERE THE WIFI CONNECTION ALGORITHM HAPP-
// ENS, YOU CAN MODIFY IT TO ALGORITHMS, SUCH AS RSSI FIRST CONNECTION, BUT IT
// IS SOMEWHAT DIFFICULT, AND I HAVN'T FIND OUT HOW TO GET THE RSSI VALUE BY 
// NOW**
//
//  ===========================================================================
//
//                                         user WIFI STA
//      [side road]                        -------------
//                                           ((*))
//
//  ===========================================================================
//
//
//		[main road]
//
//               bus ?m/s              bus ?m/s              bus ?m/s 
//               ----------->          ---------->           ----------->
//                   ((*))                 ((*))                ((*))
//               +----------+          +----------+          +-----------+
//               | AP0(ch1) |          | AP1(ch6) |          | AP2(ch11) |
//               + -------- +          + -------- +          + --------- +
//       .       |    UE0   |          |    UE1   |          |    UE2    |
//      /|\      +-----+----+          +-----+----+          +-----+-----+
//       |             .                     .                     .
//       |             .                     .                     .
//       |             .                     .                     .
//       |             .                     .                     .
//       |             .                     .                     .
//       |             .                     .                     .
//       |             .                     .                     .
//       |             .                     .                     .
//  ===========================================================================
//       |             .                     .                     .
//       |             .                     .                     .
//       |             .               +---+---+                   .
//       |              \..............|  ENB  |................../
//       |                             +---+---+
//       |                                 |
//       |                                 | LTE
//       |                                 |
//       |                             +---+---+
//       |                             |  PGW  |
//       |                             +---+---+
//       |                                 | p-p
//       |                            +----+----+
//      UDP                           | Server  |
//                                    +---------+
// 
// SPECIFICATION
//   Every bus node(or ue node, also ap node) runs in a random speed between 
// minSpeed and maxSpeed, you can specify them in the command line parameters
// by:
//     ./waf --run "scratch/multi-ap-with-lte --minSpeed=10 --maxSpeed=20"
// or some like that, besides `maxSpeed` and `minSpeed`, you can also specify 
// the following parameters:
//	 nAp               - Number of wifi networks, 3 by default
//	 minSpeed          - Minimum speed of the bus, 10 by default
//	 maxSpeed          - Maximum speed of the bus, 10 by default
//	 openWriteMobility - Write mobility trace, false by default
//	 openPcap          - Write pcap traces, false by default
//
// AUTHOR 
//   simon
//
// REFERENCE
//    1. "ns3-wifi-infra-handoff"
//		from "https://github.com/dragos-niculescu/ns3-wifi-infra-handoff"
//    2. "ns3-dhcp-new" 
//		from "https://github.com/adeepkit01/ns3-dhcp-new"
//

#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/point-to-point-helper.h"
#include "ns3/internet-module.h"
#include "ns3/wifi-module.h"
#include "ns3/lte-module.h"
#include "ns3/lte-helper.h"
#include "ns3/epc-helper.h"
#include "ns3/applications-module.h"
#include "ns3/mobility-module.h"

#include <cstdlib>
#include <sstream>
#include <string>
#include <vector>


using namespace ns3;


#define SCRIPT_NAME "MULTI-AP-WITH-LTE-SCRIPT"


NS_LOG_COMPONENT_DEFINE(SCRIPT_NAME);


typedef std::pair<Ptr<Ipv4>, uint32_t> Ipv4Pair;


#define nUe nAp
#define apNodes ueNodes
#define apStaticRoutings ueStaticRoutings


#define Ipv4Of(ipv4Pair) ((ipv4Pair).first)
#define IfIndexOf(ipv4Pair) ((ipv4Pair).second)
#define Ipv4AddressOf(ipv4Pair) (Ipv4Of(ipv4Pair)->GetAddress(IfIndexOf(ipv4Pair), 0).GetLocal())


static void SetPosition(Ptr<Node> node, const Vector& position) {
	Ptr<MobilityModel> m = node->GetObject<MobilityModel>();
	m->SetPosition(position);
}

static void SetPositionVelocity(Ptr<Node> node, const Vector& position, const Vector& velocity) {
	Ptr<ConstantVelocityMobilityModel> m = node->GetObject<ConstantVelocityMobilityModel>();
	m->SetPosition(position);
	m->SetVelocity(velocity);
}

static std::pair<Ptr<Ipv4>, uint32_t> SetIpv4Address(Ptr<NetDevice> device, const char* address, const char* mask, bool workaround) {
	Ptr<Node> node = device->GetNode ();
	Ptr<Ipv4> ipv4 = node->GetObject<Ipv4> ();
	int32_t interface = ipv4->GetInterfaceForDevice (device);
	
	if(interface == -1) {
	  interface = ipv4->AddInterface (device);
	}

	if(workaround) {
		ipv4->AddAddress(interface, Ipv4InterfaceAddress(Ipv4Address(address), Ipv4Mask("/0"))); // ???? what is workaround
	}
	ipv4->AddAddress(interface, Ipv4InterfaceAddress(Ipv4Address(address), Ipv4Mask(mask)));
	ipv4->SetMetric(interface, 1);
	ipv4->SetForwarding(interface, true);
	ipv4->SetUp(interface);	

	return std::pair<Ptr<Ipv4>, uint32_t>(ipv4, interface);
}


int main(int argc, char* argv[]) {
	uint32_t nAp = 3;
	int32_t minSpeed = 10;
	int32_t maxSpeed = 10;
	bool openWriteMobility = false;
	bool openPcap = false;


	CommandLine cmd;
	cmd.AddValue("nAp", "Number of wifi networks", nAp);
	cmd.AddValue("minSpeed", "Minimum speed of the bus", minSpeed);
	cmd.AddValue("maxSpeed", "Maximum speed of the bus", maxSpeed);
	cmd.AddValue("openWriteMobility", "Write mobility trace", openWriteMobility);
	cmd.AddValue("openPcap", "Write pcap traces", openPcap);
	cmd.Parse(argc, argv);

	if(maxSpeed < 0 || minSpeed < 0) {
		maxSpeed = minSpeed = 10;
	} else if(maxSpeed < minSpeed) {
		maxSpeed = minSpeed;
	}


	LogComponentEnable("MULTI-AP-WITH-LTE-SCRIPT", LOG_LEVEL_INFO);
	LogComponentEnable("UdpEchoClientApplication", LOG_LEVEL_INFO);
	LogComponentEnable("UdpEchoServerApplication", LOG_LEVEL_INFO);
	LogComponentEnable("DhcpClient", LOG_LEVEL_INFO);
	LogComponentEnable("DhcpServer", LOG_LEVEL_INFO);


	// 1. Nodes: create nodes
	NS_LOG_INFO("1. Nodes: create nodes");

	Ptr<Node> srvNode;
	Ptr<Node> pgwNode;
	Ptr<Node> enbNode;
	NodeContainer ueNodes;
	Ptr<Node> staNode;

	srvNode = CreateObject<Node>();
	
	Ptr<LteHelper> lteHelper = CreateObject<LteHelper>();
	Ptr<PointToPointEpcHelper> epcHelper = CreateObject<PointToPointEpcHelper>();
	lteHelper->SetEpcHelper(epcHelper);	
	pgwNode = epcHelper->GetPgwNode();
	
	enbNode = CreateObject<Node>();
	ueNodes.Create(nUe);
	staNode = CreateObject<Node>();


	// 2. Mobility
	NS_LOG_INFO("2. Mobility");

	MobilityHelper mobility;

	mobility.SetPositionAllocator("ns3::GridPositionAllocator",
								  "MinX", DoubleValue(0.0),
								  "MinY", DoubleValue(0.0),
								  "DeltaX", DoubleValue(5.0),
								  "DeltaY", DoubleValue(5.0),
								  "GridWidth", UintegerValue(1),
								  "LayoutType", StringValue("RowFirst"));

	mobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");
	mobility.Install(srvNode);
	mobility.Install(pgwNode);
	mobility.Install(enbNode);
	mobility.Install(staNode);
	mobility.SetMobilityModel("ns3::ConstantVelocityMobilityModel");
	mobility.Install(apNodes);

	srand(time(0));
	SetPosition(srvNode, Vector(0.0, 0.0, 0.0));
	SetPosition(pgwNode, Vector(0.0, 60.0, 0.0));
	SetPosition(enbNode, Vector(0.0, 120.0, 0.0));
	for(uint32_t i = 0; i < nAp; i ++) {
		SetPositionVelocity(
			apNodes.Get(i), 
			Vector(-300 + (int32_t)i * 150, 180.0, 0.0),
			Vector(minSpeed + rand() % (maxSpeed - minSpeed), 0.0, 0.0));
	}
	SetPosition(staNode, Vector(0.0, 240.0, 0.0));


	// 3. Devices: create devices
	NS_LOG_INFO("3. Devices: create devices");

	Ptr<NetDevice> srvDevice;
	Ptr<NetDevice> pgwDevice;
	Ptr<NetDevice> enbDevice;
	NetDeviceContainer ueDevices;
	NetDeviceContainer apDevices;
	Ptr<NetDevice> staDevice;

	// 3.1 srv <p-p> pgw
	NS_LOG_INFO(" 3.1 srv <p-p> pgw");

	PointToPointHelper p2pHelper;
	p2pHelper.SetDeviceAttribute("DataRate", DataRateValue (DataRate ("100Gb/s")));
	p2pHelper.SetDeviceAttribute("Mtu", UintegerValue (1500));
	p2pHelper.SetChannelAttribute("Delay", TimeValue (Seconds (0.010)));
	NetDeviceContainer devContainer = p2pHelper.Install(srvNode, pgwNode);

	srvDevice = devContainer.Get(0);
	pgwDevice = devContainer.Get(1);

	// 3.2 pgw <LTE> enb <LTE> ue
	NS_LOG_INFO(" 3.2 pgw <LTE> enb <LTE> ue");

	enbDevice = lteHelper->InstallEnbDevice(NodeContainer(enbNode)).Get(0);
	ueDevices = lteHelper->InstallUeDevice(ueNodes);
	
	// 3.3 ap(ue) <wifi> sta
	NS_LOG_INFO(" 3.3 ap(ue) <wifi> sta");

	YansWifiChannelHelper wifiChannel = YansWifiChannelHelper::Default();
	// wifiChannel.SetPropagationDelay();
	// wifiChannel.AddPropagationLoss();

	YansWifiPhyHelper wifiPhy = YansWifiPhyHelper::Default();
	wifiPhy.SetPcapDataLinkType(YansWifiPhyHelper::DLT_IEEE802_11_RADIO);
	wifiPhy.SetChannel(wifiChannel.Create());

	NqosWifiMacHelper wifiMac = NqosWifiMacHelper::Default();
	
	WifiHelper wifi = WifiHelper::Default();
	wifi.SetRemoteStationManager ("ns3::ArfWifiManager"); // do not well understood
	wifi.SetStandard(WIFI_PHY_STANDARD_80211g);

	// ap
	for(uint32_t i = 0; i < nAp; i ++) {
		std::stringstream ss; ss << "wifi-ap-sta-" << i;
		wifiMac.SetType("ns3::ApWifiMac",
			"Ssid", SsidValue(ss.str()));
		wifiPhy.Set("ChannelNumber", UintegerValue(1 + (i % 3) * 5));
		// wifiPhy.Set("ChannelNumber", UintegerValue(11));
		apDevices.Add(wifi.Install(wifiPhy, wifiMac, apNodes.Get(i)));
	}

	// sta
	wifiMac.SetType("ns3::StaWifiMac",
		"ScanType", EnumValue(StaWifiMac::ACTIVE),
		"ActiveProbing", BooleanValue(true));	
	staDevice = wifi.Install(wifiPhy, wifiMac, staNode).Get(0);


	// 4. Stack
	NS_LOG_INFO("4. Stack");

	InternetStackHelper internetStack;
	internetStack.Install(srvNode);
	internetStack.Install(ueNodes);
	internetStack.Install(staNode);
	

	// 5. Ip: we assign ip p-p based on 10.0.x.x, and wifi based 192.168.x.x
	NS_LOG_INFO("5. Ip: we assign ip p-p based on 10.0.x.x, and wifi based 192.168.x.x");
	
	Ipv4Pair srvIpv4Pair;
	Ipv4Pair pgwIpv4Pair;
	std::vector<Ipv4Pair> ueIpv4Pairs;
	std::vector<Ipv4Pair> apIpv4Pairs;

	// 5.1 srv <p-p> pgw
	NS_LOG_INFO(" 5.1 srv <p-p> pgw");

	srvIpv4Pair = SetIpv4Address(srvDevice, "10.0.0.1", "/24", false);
	pgwIpv4Pair = SetIpv4Address(pgwDevice, "10.0.0.2", "/24", false);

	// 5.2 pgw <LTE> enb <LTE> ue
	NS_LOG_INFO(" 5.2 pgw <LTE> enb <LTE> ue");

	Ipv4InterfaceContainer ueIpv4Interfaces = epcHelper->AssignUeIpv4Address(NetDeviceContainer(ueDevices));
	for(uint32_t i = 0; i < nUe; i ++) {
		ueIpv4Pairs.push_back(ueIpv4Interfaces.Get(i));
		// attach every ue to this enb
		lteHelper->Attach(ueDevices.Get(i), enbDevice);
	}

	// 5.3 ap(ue) <wifi> sta
	NS_LOG_INFO(" 5.3 ap(ue) <wifi> sta");
	
	Ipv4InterfaceContainer apIpv4Interfaces;
	for(uint32_t i = 0; i < nAp; i ++) {
		std::stringstream ip; ip << "192.168." << i << ".1";
		apIpv4Pairs.push_back(
			SetIpv4Address(apDevices.Get(i), ip.str().c_str(), "/24", true));
	}
	SetIpv4Address(staDevice, "0.0.0.0", "/0", false);


	// 6. Static routing
	NS_LOG_INFO("6. Static routing");

	Ipv4StaticRoutingHelper ipv4RoutingHelper;
	Ptr<Ipv4StaticRouting> srvStaticRouting = ipv4RoutingHelper.GetStaticRouting(Ipv4Of(srvIpv4Pair));
	Ptr<Ipv4StaticRouting> pgwStaticRouting = ipv4RoutingHelper.GetStaticRouting(Ipv4Of(pgwIpv4Pair));
	std::vector<Ptr<Ipv4StaticRouting> > ueStaticRoutings;

	// 6.1 srv -> sta: by default gateway
	NS_LOG_INFO(" 6.1 srv -> sta: by default gateway");

	srvStaticRouting->SetDefaultRoute(
		Ipv4AddressOf(pgwIpv4Pair),
		IfIndexOf(srvIpv4Pair),
		0);

	// 6.2 pgw -> sta: by default gateway
	NS_LOG_INFO(" 6.2 pgw -> sta: by default gateway");

	for(uint32_t i = 0; i < nAp; i ++) {
		pgwStaticRouting->AddNetworkRouteTo(
			Ipv4AddressOf(apIpv4Pairs[i]).CombineMask(Ipv4Mask("/24")),
			Ipv4Mask("/24"),
			Ipv4AddressOf(ueIpv4Pairs[i]),
			1); // 1 is the interface for LTE
	}

	// 6.3 sta -> srv: handled by dhcp
	NS_LOG_INFO(" 6.3 sta -> srv: handled by dhcp");

	// 6.4 ue -> srv: by default gateway
	NS_LOG_INFO(" 6.4 ue -> srv: by default gateway");

	Ptr<Ipv4StaticRouting> ueStaticRouting;
	for(uint32_t i = 0; i < nUe; i ++) {
		ueStaticRouting = ipv4RoutingHelper.GetStaticRouting(Ipv4Of(ueIpv4Pairs[i]));
		ueStaticRouting->SetDefaultRoute(
			epcHelper->GetUeDefaultGatewayAddress(), 
			IfIndexOf(ueIpv4Pairs[i]));
		ueStaticRoutings.push_back(ueStaticRouting);	
	}


	// 7. Application
	NS_LOG_INFO("7. Application");

	// 7.1 DHCP
	NS_LOG_INFO(" 7.1 DHCP");

	std::vector<ApplicationContainer> dhcpServers;
	for(uint32_t i = 0; i < nAp; i ++) {
		DhcpServerHelper dhcpServerHelper(
			Ipv4AddressOf(apIpv4Pairs[i]).CombineMask(Ipv4Mask("/24")), Ipv4Mask("/24"),
			Ipv4AddressOf(apIpv4Pairs[i]),
			Ipv4Address(Ipv4AddressOf(apIpv4Pairs[i]).Get()), 
			Ipv4Address(Ipv4AddressOf(apIpv4Pairs[i]).Get() + 253));
		dhcpServers.push_back(dhcpServerHelper.Install(apNodes.Get(i)));
		dhcpServers[i].Start(Seconds(0.0));
		dhcpServers[i].Stop(Seconds(1000.0));
	}

	DhcpClientHelper dhcpClientHelper(0);
	dhcpClientHelper.SetAttribute("Collect", TimeValue(Seconds(0.01)));
	dhcpClientHelper.SetAttribute("RTRS", TimeValue(Seconds(0.1)));
	ApplicationContainer dhcpClient = dhcpClientHelper.Install(staNode);
	dhcpClient.Start(Seconds(1.0));
	dhcpClient.Stop(Seconds(1000.0));

	// 7.2 UDP
	NS_LOG_INFO(" 7.2 UDP");

	UdpEchoServerHelper udpEchoServerHelper(9); // port 9
	ApplicationContainer udpEchoServer = udpEchoServerHelper.Install(srvNode);
	udpEchoServer.Start(Seconds(0.0));
	udpEchoServer.Stop(Seconds(1000.0));

	UdpEchoClientHelper udpEchoclientHelper(Ipv4AddressOf(srvIpv4Pair), 9); // ip server, port 9
	udpEchoclientHelper.SetAttribute("MaxPackets", UintegerValue(65536));
	udpEchoclientHelper.SetAttribute("Interval", TimeValue(Seconds(1)));
	udpEchoclientHelper.SetAttribute("PacketSize", UintegerValue(1024));
	ApplicationContainer udpEchoClient = udpEchoclientHelper.Install(apNodes.Get(nAp - 1));
	udpEchoClient.Start(Seconds(1.0));
	udpEchoClient.Stop(Seconds(1000.0));


	// 8. Start simulating
	NS_LOG_INFO("8. Start simulating");

	// write mobility
	if(openWriteMobility) {
		AsciiTraceHelper ascii;
		MobilityHelper::EnableAsciiAll(ascii.CreateFileStream(SCRIPT_NAME ".mob"));
	}

	// trace
	if(openPcap) {
		p2pHelper.EnablePcapAll(SCRIPT_NAME);
		lteHelper->EnableTraces();
	}

	Simulator::Stop(Seconds(1100));

	Simulator::Run();
	Simulator::Destroy();


	// 9. End simulating
	NS_LOG_INFO("9. End simulating");
	

	return 0;
}