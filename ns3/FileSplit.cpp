#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <iomanip>

using namespace std;

void chunkFile(char *inFile, char *outFilePrefix, unsigned long chunkSize) {
	ifstream input;
	input.open(inFile, ios::in | ios::binary);

	if (!input.is_open()) {
		cout << "Error opening file!" << endl;
		return;
	}
	
	ofstream output;
	int counter = 0;
	string outFileName;
	std::ostringstream oss;
	char *buffer = new char[chunkSize];

	while (!input.eof()) {
		oss.str("");
		oss << std::setfill('0') << std::setw(8) << counter++;

		outFileName.clear();
		outFileName.append(outFilePrefix);
		outFileName.append(".");
		outFileName.append(oss.str());
			
		output.open(outFileName.c_str(), ios::out | ios::trunc | ios::binary);
		
		if (!output.is_open()) {
			cout << "Error opening file!" << endl;
			return;
		}
		
		input.read(buffer, chunkSize);
		output.write(buffer, input.gcount());
		output.close();
		
	}

	delete(buffer);
	input.close();

	cout << "Chunking complete! " << counter << " files created." << endl;
}

int main() {
	chunkFile("D:\\\\TEST.mkv", "D:\\\\SPLIT\\TESTOUT", 20*1024*1024);
}