// The default network topology includes 1 content server, one switch(a router
// actually), 3 wifi ap installed in the bus, and 1 wifi sta(as people, the 
// user). The user stayed still on the side road, and when a bus comes here, 
// the user will connect to the very first ap(installed on the bus, which fir-
// stly responds it via DHCP**AND HERE IS WHERE THE WIFI CONNECTION ALGORITHM 
// HAPPENS, YOU CAN MODIFY IT TO ALGORITHMS, SUCH AS RSSI FIRST CONNECTION, BUT
// IT IS SOMEWHAT DIFFICULT, AND I HAVN'T FIND OUT HOW TO GET THE RSSI VALUE BY 
// NOW**
//
//  ==========================================================================
//
//                                         user WIFI STA
//      [side road]                        -------------
//                                           ((*))
//
//  ==========================================================================
//
//
//		[main road]
//
//
//              bus ?m/s                   bus ?m/s             bus ?m/s 
//              ---------->                ---------->          ----------->
//                ((*))                       ((*))                ((*))
//              +---------+                +---------+          +----------+
//       .      | AP0(ch1)|                | AP1(ch6)|          | AP2(ch11)|
//      /|\     +---------+                +---------+          +----------+
//       |            |                        |                   |
//       |            | CSMA                   | CSMA              | CSMA
//       |            |                        |                   |
//       |            |                        |                   |
//       |            |                        |                   |
//       |            |                        |                   |
//       |            |                        |                   |
//       |            |                        |                   |
//       |            |                        |                   |
//  ==========================================================================
//       |            |                        |                   |
//       |            |                        |                   |
//       |        +---+-----+                  |                   |
//       |        | Switch  |------------------+                   |
//       |        |         |--------------------------------------+
//       |        +---+-----+
//       |            | CSMA
//       |        +---+-----+
//      UDP       | Server  |
//                +---------+
// 
// AUTHOR: simon
// REFERENCE: 1. "ns3-wifi-infra-handoff"
//				 from "https://github.com/dragos-niculescu/ns3-wifi-infra-handoff"
//            2. "ns3-dhcp-new" 
//				 from "https://github.com/adeepkit01/ns3-dhcp-new"
//

#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/wifi-module.h"
#include "ns3/mobility-module.h"
#include "ns3/applications-module.h"
#include "ns3/csma-module.h"
#include "ns3/internet-module.h"

#include <iostream>
#include <sstream>
#include <string>
#include <vector>

using namespace ns3;

NS_LOG_COMPONENT_DEFINE("MULTI-AP-WITH-CSMA-SCRIPT");

static void SetPosition(Ptr<Node> node, const Vector& position) {
	Ptr<MobilityModel> m = node->GetObject<MobilityModel>();
	m->SetPosition(position);
}

// static void SetVelocity(Ptr<Node> node, const Vector& velocity) {
// 	Ptr<ConstantVelocityMobilityModel> m = node->GetObject<ConstantVelocityMobilityModel>();
// 	m->SetVelocity(velocity);
// }

static void SetPositionVelocity(Ptr<Node> node, const Vector& position, const Vector& velocity) {
	Ptr<ConstantVelocityMobilityModel> m = node->GetObject<ConstantVelocityMobilityModel>();
	m->SetPosition(position);
	m->SetVelocity(velocity);
}

static std::pair<Ptr<Ipv4>, uint32_t> SetIpv4Address(Ptr<NetDevice> device, const char* address, const char* mask, bool workaround) {
	Ptr<Node> node = device->GetNode ();
	Ptr<Ipv4> ipv4 = node->GetObject<Ipv4> ();
	int32_t interface = ipv4->GetInterfaceForDevice (device);
	
	if(interface == -1) {
	  interface = ipv4->AddInterface (device);
	}

	if(workaround) {
		ipv4->AddAddress(interface, Ipv4InterfaceAddress(Ipv4Address(address), Ipv4Mask("/0"))); // ???? what is workaround
	}
	ipv4->AddAddress(interface, Ipv4InterfaceAddress(Ipv4Address(address), Ipv4Mask(mask)));
	ipv4->SetMetric(interface, 1);
	ipv4->SetForwarding(interface, true);
	ipv4->SetUp(interface);	

	return std::pair<Ptr<Ipv4>, uint32_t>(ipv4, interface);
}

int main(int argc, char* argv[]) {
	uint32_t nAp = 2;
	// uint32_t nSta = 1;
	// bool sendIp = true;
	// bool writeMobility = true;
	// bool pcap = true;


	CommandLine cmd;
	cmd.AddValue("nAp", "Number of wifi networks", nAp);
	// cmd.AddValue("nSta", "Number of stations", nSta);
	// cmd.AddValue("sendIp", "Send Ipv4 or raw packets", sendIp);
	// cmd.AddValue("writeMobility", "Write mobility trace", writeMobility);
	// cmd.AddValue("pcap", "Write pcap traces", pcap);
	cmd.Parse(argc, argv);


	LogComponentEnable("MULTI-AP-WITH-CSMA-SCRIPT", LOG_LEVEL_INFO);
	LogComponentEnable("UdpEchoClientApplication", LOG_LEVEL_INFO);
	LogComponentEnable("UdpEchoServerApplication", LOG_LEVEL_INFO);
	LogComponentEnable("DhcpClient", LOG_LEVEL_INFO);
	LogComponentEnable("DhcpServer", LOG_LEVEL_INFO);


	// 1. Nodes: create nodes
	NS_LOG_INFO("1. Nodes: create nodes");

	NodeContainer serverNodes;
	NodeContainer switchNodes;
	NodeContainer apNodes;
	NodeContainer staNodes;

	serverNodes.Create(1);
	switchNodes.Create(1);
	apNodes.Create(nAp);
	staNodes.Create(1);


	// 2. Devices: create devices
	NS_LOG_INFO("2. Devices: create devices");

	NetDeviceContainer server2SwitchCsmaDevices;
	NetDeviceContainer switch2ServerCsmaDevices;
	NetDeviceContainer switch2ApCsmaDevices;
	std::vector<NetDeviceContainer> ap2SwitchCsmaDevices;
	std::vector<NetDeviceContainer> ap2StaWifiDevices;
	NetDeviceContainer sta2ApWifiDevices;

	// 2.1 create csma devices
	NS_LOG_INFO(" 2.1 create csma devices");

	NetDeviceContainer csmaDevices;
	CsmaHelper csmaHelper;

	// server - switch
	csmaDevices = csmaHelper.Install(NodeContainer(
		serverNodes.Get(0), switchNodes.Get(0)));
	server2SwitchCsmaDevices.Add(csmaDevices.Get(0));
	switch2ServerCsmaDevices.Add(csmaDevices.Get(1));

	// switch - ap
	for(uint32_t i = 0; i < nAp; i ++) {
		csmaDevices = csmaHelper.Install(NodeContainer(
			apNodes.Get(i), switchNodes.Get(0)));
		ap2SwitchCsmaDevices.push_back(NetDeviceContainer(csmaDevices.Get(0)));
		switch2ApCsmaDevices.Add(csmaDevices.Get(1));
	}

	// 2.2 create wifi devices
	NS_LOG_INFO(" 2.2 create wifi devices");

	YansWifiChannelHelper wifiChannel = YansWifiChannelHelper::Default();
	// wifiChannel.SetPropagationDelay();
	// wifiChannel.AddPropagationLoss();

	YansWifiPhyHelper wifiPhy = YansWifiPhyHelper::Default();
	wifiPhy.SetPcapDataLinkType(YansWifiPhyHelper::DLT_IEEE802_11_RADIO);
	wifiPhy.SetChannel(wifiChannel.Create());

	NqosWifiMacHelper wifiMac = NqosWifiMacHelper::Default();
	
	WifiHelper wifi = WifiHelper::Default();
	wifi.SetRemoteStationManager ("ns3::ArfWifiManager"); // do not well understood
	wifi.SetStandard(WIFI_PHY_STANDARD_80211g);

	// ap
	wifiMac.SetType("ns3::ApWifiMac",
					"Ssid", SsidValue(std::string("wifi-ap-sta")));
	for(uint32_t i = 0; i < nAp; i ++) {
		wifiPhy.Set("ChannelNumber", UintegerValue(1 + (i % 3) * 5));
		// wifiPhy.Set("ChannelNumber", UintegerValue(11));
		ap2StaWifiDevices.push_back(wifi.Install(wifiPhy, wifiMac, apNodes.Get(i)));
	}

	// sta
	wifiMac.SetType("ns3::StaWifiMac",
					"ScanType", EnumValue(StaWifiMac::ACTIVE),
					"ActiveProbing", BooleanValue(true));	
	sta2ApWifiDevices = wifi.Install(wifiPhy, wifiMac, staNodes.Get(0));
	

	// 3. Stack
	NS_LOG_INFO("3. Stack");

	InternetStackHelper stack;
	stack.Install(serverNodes);
	stack.Install(switchNodes);
	stack.Install(apNodes);
	stack.Install(staNodes);


/*	// 4. Ip: we assign ip csma based on 10.0.x.x, and wifi based 192.168.x.x
	Ipv4InterfaceContainer server2SwitchInterfaces;
	Ipv4InterfaceContainer switch2ServerInterfaces;
	Ipv4InterfaceContainer switch2ApInterfaces;
	std::vector<Ipv4InterfaceContainer> ap2SwitchInterfaces;
	std::vector<Ipv4InterfaceContainer> ap2StaInterfaces;
	Ipv4InterfaceContainer sta2ApInterfaces;
	Ipv4AddressHelper address;

	// server - switch
	address.SetBase("10.1.1.0", "255.255.255.0");
	server2SwitchInterfaces = address.Assign(server2SwitchCsmaDevices);
	switch2ServerInterfaces = address.Assign(switch2ServerCsmaDevices);

	// switch - ap
	for(uint32_t i = 0; i < nAp; i ++) {
		address.NewNetwork();
		switch2ApInterfaces.Add(address.Assign(switch2ApCsmaDevices.Get(i)));
		ap2SwitchInterfaces.push_back(address.Assign(ap2SwitchCsmaDevices[i]));
	}

	// ap - sta
	address.SetBase("192.168.1.0", "255.255.255.0");
	for(uint32_t i = 0; i < nAp; i ++) {
		ap2StaInterfaces.push_back(address.Assign(ap2StaWifiDevices[i]));
		address.NewNetwork();
	}
	
	// sta
	//sta2ApInterfaces = address.Assign(sta2ApWifiDevices);*/


	// 4. Ip: we assign ip csma based on 10.0.x.x, and wifi based 192.168.x.x
	NS_LOG_INFO("4. Ip: we assign ip csma based on 10.0.x.x, and wifi based 192.168.x.x");

	Ipv4InterfaceContainer server2SwitchInterfaces;
	Ipv4InterfaceContainer switch2ServerInterfaces;
	Ipv4InterfaceContainer switch2ApInterfaces;
	std::vector<Ipv4InterfaceContainer> ap2SwitchInterfaces;
	std::vector<Ipv4InterfaceContainer> ap2StaInterfaces;
	Ipv4InterfaceContainer sta2ApInterfaces;

	// server - switch
	server2SwitchInterfaces.Add(
		SetIpv4Address(server2SwitchCsmaDevices.Get(0), "10.1.1.1", "/24", false));
	switch2ServerInterfaces.Add(
		SetIpv4Address(switch2ServerCsmaDevices.Get(0), "10.1.1.2", "/24", false));

	// switch - ap
	for(uint32_t i = 0; i < nAp; i ++) {
		std::stringstream ip1; ip1 << "10.1." << (i+2) << ".1";
		std::stringstream ip2; ip2 << "10.1." << (i+2) << ".2";
		switch2ApInterfaces.Add(
			SetIpv4Address(switch2ApCsmaDevices.Get(i), ip1.str().c_str(), "/24", false));
		ap2SwitchInterfaces.push_back(Ipv4InterfaceContainer());
		ap2SwitchInterfaces[i].Add(
			SetIpv4Address(ap2SwitchCsmaDevices[i].Get(0), ip2.str().c_str(), "/24", false));
	}

	// ap - sta
	for(uint32_t i = 0; i < nAp; i ++) {
		std::stringstream ip; ip << "192.168." << (i+1) << ".1";
		ap2StaInterfaces.push_back(Ipv4InterfaceContainer());
		ap2StaInterfaces[i].Add(
			SetIpv4Address(ap2StaWifiDevices[i].Get(0), ip.str().c_str(), "/24", true));
	}

	// sta
	sta2ApInterfaces.Add(
		SetIpv4Address(sta2ApWifiDevices.Get(0), "0.0.0.0", "/0", false));


	// 5. Global route: next function is not supported on wifi-module
	// Ipv4GlobalRoutingHelper::PopulateRoutingTables();


	// 5. Static routing
	NS_LOG_INFO("5. Static routing");

	// 5.1 ipv4
	NS_LOG_INFO(" 5.1 ipv4");

	Ptr<Ipv4> serverIpv4 = serverNodes.Get(0)->GetObject<Ipv4>();
	Ptr<Ipv4> switchIpv4 = switchNodes.Get(0)->GetObject<Ipv4>();
	std::vector<Ptr<Ipv4> > apIpv4s;
	Ptr<Ipv4> staIpv4 = staNodes.Get(0)->GetObject<Ipv4>();

	for(uint32_t i = 0; i < nAp; i ++) {
		apIpv4s.push_back(apNodes.Get(i)->GetObject<Ipv4>());
	}

	// 5.2 static routing table
	NS_LOG_INFO(" 5.2 static routing table");

	Ipv4StaticRoutingHelper ipv4RoutingHelper;
	Ptr<Ipv4StaticRouting> serverStaticRouting = ipv4RoutingHelper.GetStaticRouting(serverIpv4);
	Ptr<Ipv4StaticRouting> switchStaticRouting = ipv4RoutingHelper.GetStaticRouting(switchIpv4);
	std::vector<Ptr<Ipv4StaticRouting> > apStaticRoutings;
	Ptr<Ipv4StaticRouting> staStaticRouting = ipv4RoutingHelper.GetStaticRouting(staIpv4);

	for(uint32_t i = 0; i < nAp; i ++) {
		apStaticRoutings.push_back(ipv4RoutingHelper.GetStaticRouting(apIpv4s[i]));
	}

	// server -> sta: by default gateway
	NS_LOG_INFO("  server -> sta: by default gateway");

	serverStaticRouting->SetDefaultRoute(
		switch2ServerInterfaces.GetAddress(0), 
		server2SwitchInterfaces.Get(0).second, 
		0);

	// switch -> sta
	NS_LOG_INFO("  switch -> sta");

	for(uint32_t i = 0; i < nAp; i ++) {
		switchStaticRouting->AddNetworkRouteTo(
			Ipv4Address(ap2StaInterfaces[i].GetAddress(0).CombineMask(Ipv4Mask("/24"))),
			Ipv4Mask("/24"),
			ap2SwitchInterfaces[i].GetAddress(0),
			switch2ApInterfaces.Get(i).second);
	}

	// sta -> server: managed by dhcp
	NS_LOG_INFO("  sta -> server: managed by dhcp");

	// do nothing

	// ap -> server
	NS_LOG_INFO("  ap -> server");

	for(uint32_t i = 0; i < nAp; i ++) {
		apStaticRoutings[i]->AddHostRouteTo(
			server2SwitchInterfaces.GetAddress(0), 
			switch2ApInterfaces.GetAddress(i), 
			ap2SwitchInterfaces[i].Get(0).second);
	}


	// 6. Application
	NS_LOG_INFO("6. Application");

	// 6.1 DHCP
	NS_LOG_INFO(" 6.1 DHCP");

	std::vector<ApplicationContainer> dhcpServers;
	for(uint32_t i = 0; i < nAp; i ++) {
		DhcpServerHelper dhcpServerHelper(
			ap2StaInterfaces[i].GetAddress(0).CombineMask(Ipv4Mask("/24")), Ipv4Mask("/24"),
			ap2StaInterfaces[i].GetAddress(0),
			Ipv4Address(ap2StaInterfaces[i].GetAddress(0).Get()), 
			Ipv4Address(ap2StaInterfaces[i].GetAddress(0).Get() + 253));
		dhcpServers.push_back(dhcpServerHelper.Install(apNodes.Get(i)));
		dhcpServers[i].Start(Seconds(0.0));
		dhcpServers[i].Stop(Seconds(1000.0));
	}

	DhcpClientHelper dhcpClientHelper(0);
	dhcpClientHelper.SetAttribute("Collect", TimeValue(Seconds(0.01)));
	dhcpClientHelper.SetAttribute("RTRS", TimeValue(Seconds(0.1)));
	ApplicationContainer dhcpClient = dhcpClientHelper.Install(staNodes.Get(0));
	dhcpClient.Start(Seconds(1.0));
	dhcpClient.Stop(Seconds(1000.0));

	// 6.2 UDP
	NS_LOG_INFO(" 6.2 UDP");

	UdpEchoServerHelper udpEchoServerHelper(9); // port 9
	ApplicationContainer udpEchoServer = udpEchoServerHelper.Install(serverNodes.Get(0));
	udpEchoServer.Start(Seconds(0.0));
	udpEchoServer.Stop(Seconds(1000.0));

	UdpEchoClientHelper udpEchoclientHelper(server2SwitchInterfaces.GetAddress(0), 9); // ip server, port 9
	udpEchoclientHelper.SetAttribute("MaxPackets", UintegerValue(65536));
	udpEchoclientHelper.SetAttribute("Interval", TimeValue(Seconds(0.1)));
	udpEchoclientHelper.SetAttribute("PacketSize", UintegerValue(1024));
	ApplicationContainer udpEchoClient = udpEchoclientHelper.Install(staNodes.Get(0));
	udpEchoClient.Start(Seconds(1.0));
	udpEchoClient.Stop(Seconds(1000.0));


	// 7. Mobility
	NS_LOG_INFO("7. Mobility");

	MobilityHelper mobility;

	mobility.SetPositionAllocator("ns3::GridPositionAllocator",
								  "MinX", DoubleValue(0.0),
								  "MinY", DoubleValue(0.0),
								  "DeltaX", DoubleValue(5.0),
								  "DeltaY", DoubleValue(5.0),
								  "GridWidth", UintegerValue(1),
								  "LayoutType", StringValue("RowFirst"));

	mobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");
	mobility.Install(serverNodes);
	mobility.Install(switchNodes);
	mobility.Install(staNodes);
	mobility.SetMobilityModel("ns3::ConstantVelocityMobilityModel");
	mobility.Install(apNodes);

	SetPosition(serverNodes.Get(0), Vector(0.0, 0.0, 0.0));
	SetPosition(switchNodes.Get(0), Vector(0.0, 60.0, 0.0));
	for(uint32_t i = 0; i < nAp; i ++) {
		SetPositionVelocity(
			apNodes.Get(i), 
			Vector(-300 + (int32_t)i * 150, 120.0, 0.0),
			Vector(20.0, 0.0, 0.0));
	}
	SetPosition(staNodes.Get(0), Vector(0.0, 180.0, 0.0));


	// 8. Start simulating
	NS_LOG_INFO("8. Start simulating");

	Simulator::Stop(Seconds(1100));

	Simulator::Run();
	Simulator::Destroy();


	// 9. End simulating
	NS_LOG_INFO("9. End simulating");
	

	return 0;
}