#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <iomanip>

using namespace std;

int getFileSize(ifstream *in) {
	in->seekg(0, ios::end);
	int filesize = in->tellg();
	in->seekg(ios::beg);
	return filesize;
}

void joinFile(char *inFilePrefix, char *outFile, unsigned int chunkNum) {
	
	ofstream output;
	output.open(outFile, ios::out | ios::binary);

	if (!output.is_open()) {
		cout << "Error openning file!" << endl;
		return;
	}
	
	ifstream input;
	unsigned int counter = 0;
	int fileSize = 0;
	string inFileName;
	std::ostringstream oss;

	while (counter < chunkNum) {
		oss.str("");
		oss << std::setfill('0') << std::setw(8) << counter++;
			
		inFileName.clear();
		inFileName.append(inFilePrefix);
		inFileName.append(".");
		inFileName.append(oss.str());

		cout << inFileName << endl;


		input.open(inFileName.c_str(), ios::in | ios::binary);

		if (!input.is_open()) {
			cout << "Error opening file!" << endl;
			return;
		}

		fileSize = getFileSize(&input);
		char *buffer = new char[fileSize];

		input.read(buffer, fileSize);
		output.write(buffer, fileSize);
		
		delete(buffer);
		input.close();
	}
	
	output.close();

	cout << "File assembly complete!" << endl;
}

int main() {
	joinFile("D:\\\\SPLIT\\TESTOUT", "D:\\\\TEST2.mkv", 22);
}