// AUTHOR: simon
// REFERENCE: 1. "ns3-wifi-infra-handoff"
//				 from "https://github.com/dragos-niculescu/ns3-wifi-infra-handoff"
//            2. "ns3-dhcp-new"
//				 from "https://github.com/adeepkit01/ns3-dhcp-new"

#include "ns3/dce-module.h"
#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/wifi-module.h"
#include "ns3/mobility-module.h"
#include "ns3/applications-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/internet-module.h"
#include "ns3/netanim-module.h"

using namespace ns3;

NS_LOG_COMPONENT_DEFINE("NetroadMptcp");

NodeContainer staNodes;
NetDeviceContainer sta2apDevs;
std::vector<struct APInfo> aps;
Mac48Address ap1 = Mac48Address("00:00:00:00:00:00");
Mac48Address ap2 = Mac48Address("00:00:00:00:00:00");

struct APInfo
{
	Mac48Address m_mac;
	Ipv4Address m_gw;
	Ipv4Address m_ip;
	Ipv4Address m_net;

	APInfo (Mac48Address mac, Ipv4Address gw, Ipv4Address ip, Ipv4Address net)
	:m_mac(mac), m_gw(gw), m_ip(ip), m_net(net){}
};

static void
If1Assoc (Mac48Address address);

static void
If2Assoc (Mac48Address address);

static void
SetPosition(Ptr<Node> node, const Vector& position);

static void
SetPositionVelocity(Ptr<Node> node, const Vector& position, const Vector& velocity);

static Ipv4Address
RemoveIpv4Address(Ptr<NetDevice> device);

static std::pair<Ptr<Ipv4>, uint32_t>
SetIpv4Address(Ptr<NetDevice> device, const char* address, const char* mask);

int
main(int argc, char* argv[])
{
	uint32_t nApsV = 2;
	uint32_t nApsH = 2;

	CommandLine cmd;
	cmd.Parse(argc, argv);

	LogComponentEnable("NetroadMptcp", LOG_LEVEL_ALL);

	std::ostringstream oss;

	NS_LOG_INFO ("create nodes");

	NodeContainer srvNodes, swNodes, apNodes, apNodesV, apNodesH;
	srvNodes.Create(1);
	swNodes.Create(1);
	apNodes.Create(nApsV + nApsH);
	staNodes.Create(1);

	for(uint32_t i = 0; i < nApsV; i ++)
	{
		apNodesV.Add(apNodes.Get(i));
	}

	for(uint32_t i = nApsV; i < nApsV + nApsH; i ++)
	{
		apNodesH.Add(apNodes.Get(i));
	}

	NS_LOG_INFO ("mobility");

	MobilityHelper mobility;
	mobility.SetPositionAllocator("ns3::GridPositionAllocator",
																"MinX", DoubleValue(0.0),
																"MinY", DoubleValue(0.0),
																"DeltaX", DoubleValue(5.0),
																"DeltaY", DoubleValue(5.0),
																"GridWidth", UintegerValue(5),
																"LayoutType", StringValue("RowFirst"));

	mobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");
	mobility.Install(srvNodes);
	mobility.Install(swNodes);
	mobility.Install(staNodes);
	SetPosition(srvNodes.Get(0), Vector(0, 0, 0));
	SetPosition(swNodes.Get(0), Vector(40, 40, 0));
	SetPosition(staNodes.Get(0), Vector(100, 100, 0));

	mobility.SetMobilityModel("ns3::ConstantVelocityMobilityModel");
	mobility.Install(apNodes);
	for(uint32_t i = 0; i < nApsV; i ++)
	{
		SetPositionVelocity(apNodesV.Get(i), Vector(80, i * 150, 0), Vector(0, 20, 0));
	}

	for(uint32_t i = 0; i < nApsH; i ++)
	{
		SetPositionVelocity(apNodesH.Get(i), Vector(i * 150, 80, 0), Vector(20, 0, 0));
	}

	NS_LOG_INFO ("install stack");

	LinuxStackHelper stack;
	stack.Install(srvNodes);
	stack.Install(swNodes);
	stack.Install(apNodes);
	stack.Install(staNodes);

	DceManagerHelper dceManager;
	dceManager.SetTaskManagerAttribute ("FiberManagerType",
																			 StringValue("UcontextFiberManager"));
	dceManager.SetNetworkStack ("ns3::LinuxSocketFdFactory",
															"Library", StringValue("liblinux.so"));
														  dceManager.SetNetworkStack ("ns3::LinuxSocketFdFactory",
														                              "Library", StringValue ("liblinux.so"));
	dceManager.Install(srvNodes);
	dceManager.Install(swNodes);
	dceManager.Install(apNodes);
	dceManager.Install(staNodes);

	NS_LOG_INFO ("install devices");

	NetDeviceContainer srv2swDevs, sw2srvDevs, sw2apDevs, ap2swDevs, ap2staDevs;

	PointToPointHelper p2p;

	NetDeviceContainer p2pDevs = p2p.Install(NodeContainer(srvNodes.Get(0), swNodes.Get(0)));
	srv2swDevs.Add(p2pDevs.Get(0));
	sw2srvDevs.Add(p2pDevs.Get(1));

	for(uint32_t i = 0; i < nApsV + nApsH; i++)
	{
		p2pDevs = p2p.Install(NodeContainer(apNodes.Get(i), swNodes.Get(0)));
		ap2swDevs.Add(p2pDevs.Get(0));
		sw2apDevs.Add(p2pDevs.Get(1));
	}

	p2p.EnablePcapAll ("netroad-mptcp-p2p");

	YansWifiChannelHelper wifiChannel = YansWifiChannelHelper::Default();

	YansWifiPhyHelper wifiPhy = YansWifiPhyHelper::Default();
	wifiPhy.SetPcapDataLinkType(YansWifiPhyHelper::DLT_IEEE802_11_RADIO);
	wifiPhy.SetChannel(wifiChannel.Create());

	NqosWifiMacHelper wifiMac = NqosWifiMacHelper::Default();

	WifiHelper wifi = WifiHelper::Default();
	wifi.SetRemoteStationManager ("ns3::ArfWifiManager");
	wifi.SetStandard(WIFI_PHY_STANDARD_80211g);

	Ssid ssidV = Ssid("NETROAD-V");
	wifiMac.SetType("ns3::ApWifiMac",
									"Ssid", SsidValue(ssidV));
	for(uint32_t i = 0; i < nApsV; i ++)
	{
		wifiPhy.Set("ChannelNumber", UintegerValue(1 + (i % 3) * 5));
		ap2staDevs.Add(wifi.Install(wifiPhy, wifiMac, apNodesV.Get(i)));
	}

	Ssid ssidH = Ssid("NETROAD-H");
	wifiMac.SetType("ns3::ApWifiMac",
									"Ssid", SsidValue(ssidH));
	for(uint32_t i = 0; i < nApsH; i++)
	{
		wifiPhy.Set("ChannelNumber", UintegerValue(1 + (i % 3) * 5));
		ap2staDevs.Add(wifi.Install(wifiPhy, wifiMac, apNodesH.Get(i)));
	}

	wifiMac.SetType("ns3::StaWifiMac",
									"Ssid", SsidValue (ssidV),
									"ScanType", EnumValue(StaWifiMac::ACTIVE),
									"ActiveProbing", BooleanValue(true));
	sta2apDevs.Add(wifi.Install(wifiPhy, wifiMac, staNodes.Get(0)));

	oss.str("");
	oss << "/NodeList/" << staNodes.Get(0)->GetId () << "/DeviceList/0/$ns3::WifiNetDevice/Mac/$ns3::StaWifiMac/Assoc";
	Config::ConnectWithoutContext(oss.str().c_str(), MakeCallback(&If1Assoc));

	wifiMac.SetType("ns3::StaWifiMac",
									"Ssid", SsidValue (ssidH),
									"ScanType", EnumValue(StaWifiMac::ACTIVE),
									"ActiveProbing", BooleanValue(true));
	sta2apDevs.Add(wifi.Install(wifiPhy, wifiMac, staNodes.Get(0)));

	oss.str("");
	oss << "/NodeList/" << staNodes.Get(0)->GetId () << "/DeviceList/1/$ns3::WifiNetDevice/Mac/$ns3::StaWifiMac/Assoc";
	Config::ConnectWithoutContext(oss.str().c_str(), MakeCallback(&If2Assoc));

	wifiPhy.EnablePcapAll("netroad-mptcp-wifi", true);

	NS_LOG_INFO ("assign ip");

	Ipv4InterfaceContainer srv2swIfaces, sw2srvIfaces, sw2apIfaces, ap2swIfaces, ap2staIfaces, sta2apIfaces;

	srv2swIfaces.Add(SetIpv4Address(srv2swDevs.Get(0), "10.1.1.1", "/24"));
	sw2srvIfaces.Add(SetIpv4Address(sw2srvDevs.Get(0), "10.1.1.2", "/24"));

	for(uint32_t i = 0; i < nApsV + nApsH; i++)
	{
		oss.str("");
		oss << "10.1." << (i+2) << ".1";
		sw2apIfaces.Add(SetIpv4Address(sw2apDevs.Get(i), oss.str().c_str(), "/24"));

		oss.str("");
		oss << "10.1." << (i+2) << ".2";
		ap2swIfaces.Add(SetIpv4Address(ap2swDevs.Get(i), oss.str().c_str(), "/24"));

		oss.str("");
		oss << "192.168." << (i+1) << ".1";
		ap2staIfaces.Add(SetIpv4Address(ap2staDevs.Get(i), oss.str().c_str(), "/24"));

		Ipv4Address gw = Ipv4Address (oss.str().c_str());

		oss.str("");
		oss << "192.168." << (i+1) << ".2";
		Ipv4Address ip = Ipv4Address (oss.str().c_str());
		Ipv4Address net = ip.CombineMask(Ipv4Mask("/24"));

		Ptr<WifiNetDevice> wifiDev = DynamicCast<WifiNetDevice>(ap2staDevs.Get(i));
		aps.push_back(APInfo(wifiDev->GetMac()->GetAddress(), gw, ip, net));
	}

	NS_LOG_INFO ("routing");

	oss.str("");
	oss << "route add default via " << sw2srvIfaces.GetAddress(0) << " dev sim0";
	LinuxStackHelper::RunIp (srvNodes.Get(0), Seconds(0.1), oss.str());
	NS_LOG_INFO ("srv: " << oss.str().c_str());

	oss.str("");
	oss << "route add 10.1.1.0/24 via 10.1.1.2 dev sim0";
	LinuxStackHelper::RunIp (swNodes.Get(0), Seconds(0.1), oss.str());
	NS_LOG_INFO ("sw: " << oss.str().c_str());

	for(uint32_t i = 0; i < nApsV + nApsH; i ++){
		oss.str("");
		oss << "route add 192.168." << (i+1) << ".0/24 via " << ap2swIfaces.GetAddress(i) << " dev sim" << (i+1);
		// oss << "route add 192.168." << (i+1) << ".0/24 via " << sw2apIfaces.GetAddress(i) << " dev sim" << (i+1);
		LinuxStackHelper::RunIp (swNodes.Get(0), Seconds(0.1), oss.str());
		NS_LOG_INFO ("sw: " << oss.str().c_str());

		oss.str ("");
		oss << "route add 10.1.1.0/24 via " << sw2apIfaces.GetAddress(i) << " dev sim0";
		// oss << "route add 10.1.1.0/24 via " << ap2swIfaces.GetAddress(i) << " dev sim0";
		LinuxStackHelper::RunIp (apNodes.Get(i), Seconds(0.1), oss.str());
		NS_LOG_INFO ("ap: " << oss.str().c_str());

		LinuxStackHelper::RunIp (apNodes.Get (i), Seconds (10.0), "route show");
	}

	LinuxStackHelper::RunIp (staNodes.Get (0), Seconds (10.0), "rule show");
	LinuxStackHelper::RunIp (staNodes.Get (0), Seconds (10.0), "route show");
	LinuxStackHelper::RunIp (staNodes.Get (0), Seconds (10.0), "route show table 1");
	LinuxStackHelper::RunIp (staNodes.Get (0), Seconds (10.0), "route show table 2");

	ApplicationContainer apps;

	DceApplicationHelper dce;
	dce.SetStackSize (1 << 20);

	dce.SetBinary ("iperf");
	dce.ResetArguments ();
	dce.ResetEnvironment ();
	dce.AddArgument ("-s");
	dce.AddArgument ("-P");
	dce.AddArgument ("1");

	apps = dce.Install (srvNodes.Get (0));
	apps.Start (Seconds (1.0));

	dce.SetBinary ("iperf");
	dce.ResetArguments ();
	dce.ResetEnvironment ();
	dce.AddArgument ("-c");
	dce.AddArgument ("10.1.1.1");
	dce.AddArgument ("-i");
	dce.AddArgument ("1");
	dce.AddArgument ("--time");
	dce.AddArgument ("100");

	apps = dce.Install (staNodes.Get (0));
	apps.Start (Seconds (5.0));
	apps.Stop (Seconds (200));



	NS_LOG_INFO ("animation");

	AnimationInterface anim("netroad-mptcp.xml");

	Simulator::Stop(Seconds(20));
	Simulator::Run();
	Simulator::Destroy();
	return 0;
}

static void
If1Assoc (Mac48Address address)
{
	NS_LOG_INFO ("if1 assoc " << address);
	if(address == ap1)
		return;

	ap1 = address;

	for(int i = 0; i < aps.size(); i ++)
	{
		if(aps[i].m_mac != address)
			continue;

		std::ostringstream oss;

		Ipv4Address oldAddr = RemoveIpv4Address (sta2apDevs.Get(0));
		NS_LOG_INFO ("old addr: " << oldAddr);
		NS_LOG_INFO ("new addr: " << aps[i].m_ip);

		if(oldAddr != Ipv4Address("0.0.0.0"))
		{
			LinuxStackHelper::RunIp (staNodes.Get(0), Simulator::Now(), "rule del lookup 1");
			LinuxStackHelper::RunIp (staNodes.Get(0), Simulator::Now(), "route flush table 1");
		}

		oss.str ("");
		oss << aps[i].m_ip;
		SetIpv4Address(sta2apDevs.Get(0), oss.str().c_str(), "/24");

		oss.str ("");
    oss << "rule add from " << aps[i].m_ip << " table 1";
    LinuxStackHelper::RunIp (staNodes.Get (0), Simulator::Now(), oss.str ());

		oss.str ("");
    oss << "route add " << aps[i].m_net << "/24 dev sim0 scope link table 1";
    LinuxStackHelper::RunIp (staNodes.Get (0), Simulator::Now(), oss.str ());

		oss.str ("");
    oss << "route add default via " << aps[i].m_gw << " dev sim0 table 1";
    LinuxStackHelper::RunIp (staNodes.Get (0), Simulator::Now(), oss.str ());

		LinuxStackHelper::RunIp (staNodes.Get(0), Simulator::Now(), "route del default");

		oss.str("");
		oss << "route add default scope global nexthop via "<< aps[i].m_gw <<" dev sim0";
		LinuxStackHelper::RunIp (staNodes.Get(0), Simulator::Now(), oss.str());
		NS_LOG_INFO (oss.str());

		LinuxStackHelper::RunIp (staNodes.Get(0), Simulator::Now(), "route show");

		break;
	}
}

static void
If2Assoc (Mac48Address address)
{
	if(address == ap2)
		return;

	ap2 = address;

	for(int i = 0; i < aps.size(); i ++)
	{
		NS_LOG_INFO ("if2 assoc " << address);
		if(aps[i].m_mac != address)
			continue;

		std::ostringstream oss;

		Ipv4Address oldAddr = RemoveIpv4Address (sta2apDevs.Get(1));
		NS_LOG_INFO ("old addr: " << oldAddr);
		NS_LOG_INFO ("new addr: " << aps[i].m_ip);

		if(oldAddr != Ipv4Address("0.0.0.0"))
		{
			LinuxStackHelper::RunIp (staNodes.Get(0), Simulator::Now(), "rule del lookup 2");
			LinuxStackHelper::RunIp (staNodes.Get(0), Simulator::Now(), "route flush table 2");
		}

		oss.str ("");
		oss << aps[i].m_ip;
		SetIpv4Address(sta2apDevs.Get(1), oss.str().c_str(), "/24");

		oss.str ("");
    oss << "rule add from " << aps[i].m_ip << " table 2";
    LinuxStackHelper::RunIp (staNodes.Get (0), Simulator::Now(), oss.str ());

		oss.str ("");
    oss << "route add " << aps[i].m_net << "/24 dev sim1 scope link table 2";
    LinuxStackHelper::RunIp (staNodes.Get (0), Simulator::Now(), oss.str ());

		oss.str ("");
    oss << "route add default via " << aps[i].m_gw << " dev sim1 table 2";
    LinuxStackHelper::RunIp (staNodes.Get (0), Simulator::Now(), oss.str ());

		LinuxStackHelper::RunIp (staNodes.Get(0), Simulator::Now(), "route del default");

		oss.str("");
		oss << "route add default scope global nexthop via "<< aps[i].m_gw <<" dev sim1";
		LinuxStackHelper::RunIp (staNodes.Get(0), Simulator::Now(), oss.str());
		NS_LOG_INFO (oss.str());

		LinuxStackHelper::RunIp (staNodes.Get(0), Simulator::Now(), "route show");

		break;
	}
}

static void
SetPosition(Ptr<Node> node, const Vector& position)
{
	Ptr<MobilityModel> m = node->GetObject<MobilityModel>();
	m->SetPosition(position);
}

static void
SetPositionVelocity(Ptr<Node> node, const Vector& position, const Vector& velocity)
{
	Ptr<ConstantVelocityMobilityModel> m = node->GetObject<ConstantVelocityMobilityModel>();
	m->SetPosition(position);
	m->SetVelocity(velocity);
}

static Ipv4Address
RemoveIpv4Address(Ptr<NetDevice> device) {
	Ptr<Node> node = device->GetNode ();
	Ptr<Ipv4> ipv4 = node->GetObject<Ipv4> ();
	int32_t interface = ipv4->GetInterfaceForDevice (device);

	NS_LOG_INFO (interface);
	if(interface == -1){
		NS_LOG_INFO ("no address to remove");
		return Ipv4Address ("0.0.0.0");
	}

	Ipv4Address old = ipv4->GetAddress(interface, 0).GetLocal();
	ipv4->RemoveAddress(interface, 0);

	return old;
}

static std::pair<Ptr<Ipv4>, uint32_t>
SetIpv4Address(Ptr<NetDevice> device, const char* address, const char* mask) {
	Ptr<Node> node = device->GetNode ();
	Ptr<Ipv4> ipv4 = node->GetObject<Ipv4> ();
	int32_t interface = ipv4->GetInterfaceForDevice (device);

	if(interface == -1)
	{
		interface = ipv4->AddInterface (device);
	}

	ipv4->AddAddress(interface, Ipv4InterfaceAddress(Ipv4Address(address), Ipv4Mask(mask)));
	ipv4->SetMetric(interface, 1);
	ipv4->SetForwarding(interface, true);
	ipv4->SetUp(interface);

	return std::pair<Ptr<Ipv4>, uint32_t>(ipv4, interface);
}
