/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */

#include "ns3/network-module.h"
#include "ns3/core-module.h"
#include "ns3/internet-module.h"
#include "ns3/dce-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/wifi-module.h"
#include "ns3/lte-module.h"
#include "ns3/mobility-module.h"
#include "ns3/applications-module.h"
#include "ns3/netanim-module.h"
#include "ns3/constant-position-mobility-model.h"
#include "ns3/config-store-module.h"
#include "ns3/netanim-module.h"

using namespace ns3;
NS_LOG_COMPONENT_DEFINE ("DceMptcpLteWifi");

void
setPos (Ptr<Node> n, int x, int y, int z)
{
  Ptr<ConstantPositionMobilityModel> loc = CreateObject<ConstantPositionMobilityModel> ();
  n->AggregateObject (loc);
  Vector locVec2 (x, y, z);
  loc->SetPosition (locVec2);
}

void
PrintTcpFlags (std::string key, std::string value)
{
  NS_LOG_INFO (key << "=" << value);
}

int
main (int argc, char *argv[])
{
  LogComponentEnable ("DceMptcpLteWifi", LOG_LEVEL_ALL);
  GlobalValue::Bind ("ChecksumEnabled", BooleanValue (true));

  /********************************************************************************/

  /******************** create nodes **********************************************/

  NS_LOG_INFO("err1");

  NodeContainer staNodes, apNodes, serverNodes, enbNodes, pgwNodes;
  staNodes.Create(1);
  apNodes.Create(1);
  serverNodes.Create(1);

  enbNodes.Create(1);
  Ptr<PointToPointEpcHelper> epcHelper = CreateObject<PointToPointEpcHelper> ();
  pgwNodes = epcHelper->GetPgwNode();

  /******************** mobility **************************************************/
  NS_LOG_INFO("err2");

  setPos (staNodes.Get (0), -20, 30 / 2, 0);
  setPos (serverNodes.Get (0), 100, 30 / 2, 0);
  setPos (enbNodes.Get (0), 60, -15, 0);
  setPos (pgwNodes.Get(0), 70, 0, 0);
  setPos (apNodes.Get (0), 70, 15, 0);

  Ptr<RateErrorModel> em1 =
    CreateObjectWithAttributes<RateErrorModel> ("RanVar", StringValue ("ns3::UniformRandomVariable[Min=0.0,Max=1.0]"),
                                                "ErrorRate", DoubleValue (0.01),
                                                "ErrorUnit", EnumValue (RateErrorModel::ERROR_UNIT_PACKET)
                                                );

  /******************** stack *****************************************************/

  NS_LOG_INFO("err3-1");





  DceManagerHelper dceManager;
  dceManager.SetNetworkStack ("ns3::LinuxSocketFdFactory",
                              "Library", StringValue ("liblinux.so"));

  NS_LOG_INFO("err3-2");


  LinuxStackHelper stack;
  stack.Install (staNodes);
  stack.Install (apNodes);
  stack.Install (serverNodes);

  NS_LOG_INFO("err3-3");

  dceManager.Install (staNodes);
  dceManager.Install (apNodes);
  dceManager.Install (serverNodes);

  NS_LOG_INFO("err3-4");

  /******************** devices ***************************************************/

  NS_LOG_INFO("err4");

  NetDeviceContainer staWiFiDevs, apWiFiDevs, ueLteDevs, enbLteDevs, apP2PDevs, serverApDevs, pgwServerDevs, serverPgwDevs;

  WifiHelper wifi = WifiHelper::Default();
  wifi.SetStandard(WIFI_PHY_STANDARD_80211a);

  YansWifiChannelHelper phyChannel = YansWifiChannelHelper::Default ();
  YansWifiPhyHelper phy = YansWifiPhyHelper::Default ();
  phy.SetChannel (phyChannel.Create ());

//  NqosWifiMacHelper mac = NqosWifiMacHelper::Default();
  NqosWifiMacHelper mac;
  mac.SetType("ns3::AdhocWifiMac");

  staWiFiDevs = wifi.Install(phy, mac, staNodes);
  apWiFiDevs = wifi.Install(phy, mac, apNodes);

  PointToPointHelper p2p;
  p2p.SetDeviceAttribute ("DataRate", StringValue ("2Mbps"));
  p2p.SetChannelAttribute ("Delay", StringValue ("2ms"));

  NetDeviceContainer p2pPgwServerDevs = p2p.Install(pgwNodes.Get(0), serverNodes.Get(0));
  pgwServerDevs.Add(p2pPgwServerDevs.Get(0));
  serverPgwDevs.Add(p2pPgwServerDevs.Get(1));
//  serverPgwDevs.Get (0)->SetAttribute ("ReceiveErrorModel", PointerValue (em1));

  NetDeviceContainer p2pApServerDevs = p2p.Install(apNodes.Get(0), serverNodes.Get(0));
  apP2PDevs.Add(p2pApServerDevs.Get(0));
  serverApDevs.Add(p2pApServerDevs.Get(1));
//  serverApDevs.Get (0)->SetAttribute ("ReceiveErrorModel", PointerValue (em1));

  Ptr<LteHelper> lteHelper = CreateObject<LteHelper> ();
  lteHelper->SetEpcHelper (epcHelper);
  enbLteDevs = lteHelper->InstallEnbDevice (enbNodes);
  ueLteDevs = lteHelper->InstallUeDevice (staNodes.Get (0));

  /******************** ip and routing ********************************************/

  NS_LOG_INFO("err5");

  std::ostringstream cmd_oss;

  Ipv4AddressHelper pgwServerAddr, wifiAddr, apServerAddr;

  Ipv4InterfaceContainer ueLteIfaces, p2pPgwServerIfaces, p2pApServerIfaces, wifiApIfaces, wifiStaIfaces;

  // ue <-> enb
  ueLteIfaces = epcHelper->AssignUeIpv4Address (NetDeviceContainer (ueLteDevs));
  lteHelper->Attach (ueLteDevs.Get(0), enbLteDevs.Get(0));

  cmd_oss.str ("");
  cmd_oss << "rule add from " << ueLteIfaces.GetAddress (0) << " table 1";
  LinuxStackHelper::RunIp (staNodes.Get (0), Seconds (0.1), cmd_oss.str ().c_str ());
  cmd_oss.str ("");
  cmd_oss << "route add default via 7.0.0.1 dev sim0 table 1";
  LinuxStackHelper::RunIp (staNodes.Get (0), Seconds (0.1), cmd_oss.str ().c_str ());

  // pgw <-> server
  pgwServerAddr.SetBase ("10.1.0.0", "255.255.255.0");
  p2pPgwServerIfaces = pgwServerAddr.Assign (p2pPgwServerDevs);

  cmd_oss.str ("");
  cmd_oss << "rule add from " << p2pPgwServerIfaces.GetAddress(1) << " table 1";
  LinuxStackHelper::RunIp (serverNodes.Get (0), Seconds (0.1), cmd_oss.str ().c_str ());
  cmd_oss.str ("");
  cmd_oss << "route add 10.1.0.0/24 dev sim0 scope link table 1";
  LinuxStackHelper::RunIp (serverNodes.Get (0), Seconds (0.1), cmd_oss.str ().c_str ());

  // sta <-> ap
  wifiAddr.SetBase ("10.2.0.0", "255.255.255.0");
  wifiStaIfaces = wifiAddr.Assign (staWiFiDevs);
  wifiApIfaces = wifiAddr.Assign (apWiFiDevs);

  cmd_oss.str ("");
  cmd_oss << "rule add from " << wifiStaIfaces.GetAddress (0) << " table 2";
  LinuxStackHelper::RunIp (staNodes.Get(0), Seconds (0.1), cmd_oss.str ().c_str ());
  cmd_oss.str ("");
  cmd_oss << "route add 10.2.0.0/24 dev sim"
          << staWiFiDevs.Get (0)->GetIfIndex () << " scope link table 2";
  LinuxStackHelper::RunIp (staNodes.Get (0), Seconds (0.1), cmd_oss.str ().c_str ());
  cmd_oss.str ("");
  cmd_oss << "route add default via " << wifiApIfaces.GetAddress (0) << " dev sim"
          << staWiFiDevs.Get (0)->GetIfIndex () << " table 2";
  LinuxStackHelper::RunIp (staNodes.Get (0), Seconds (0.1), cmd_oss.str ().c_str ());

  cmd_oss.str ("");
  cmd_oss << "route add 10.2.0.0/16 via " << wifiStaIfaces.GetAddress (0) << " dev sim0";
  LinuxStackHelper::RunIp (apNodes.Get (0), Seconds (0.2), cmd_oss.str ().c_str ());

  // ap <-> server
  apServerAddr.SetBase ("10.3.0.0", "255.255.255.0");
  p2pApServerIfaces = apServerAddr.Assign (p2pApServerDevs);

  cmd_oss.str ("");
  cmd_oss << "rule add from " << p2pApServerIfaces.GetAddress (1) << " table 2";
  LinuxStackHelper::RunIp (serverNodes.Get (0), Seconds (0.1), cmd_oss.str ().c_str ());
  cmd_oss.str ("");
  cmd_oss << "route add 10.3.0.0/24 dev sim1 scope link table 2";
  LinuxStackHelper::RunIp (serverNodes.Get (0), Seconds (0.1), cmd_oss.str ().c_str ());
  cmd_oss.str ("");
  cmd_oss << "route add 10.2.0.0/16 via " << p2pApServerIfaces.GetAddress (0) << " dev sim1 table 2";
  LinuxStackHelper::RunIp (serverNodes.Get (0), Seconds (0.1), cmd_oss.str ().c_str ());

  cmd_oss.str ("");
  cmd_oss << "route add 10.3.0.0/16 via " << p2pApServerIfaces.GetAddress (0) << " dev sim1";
  LinuxStackHelper::RunIp (apNodes.Get (0), Seconds (0.2), cmd_oss.str ().c_str ());

  // default route
  LinuxStackHelper::RunIp (staNodes.Get (0), Seconds (0.1), "route add default via 7.0.0.1 dev sim0");
  LinuxStackHelper::RunIp (serverNodes.Get (0), Seconds (0.1), "route add default via 10.2.0.2 dev sim0");
  LinuxStackHelper::RunIp (staNodes.Get (0), Seconds (0.1), "rule show");
  LinuxStackHelper::RunIp (staNodes.Get (0), Seconds (5.1), "route show table all");
  LinuxStackHelper::RunIp (serverNodes.Get (0), Seconds (0.1), "rule show");
  LinuxStackHelper::RunIp (serverNodes.Get (1), Seconds (5.1), "route show table all");

  NS_LOG_INFO("err6");

  // debug
  stack.SysctlSet (staNodes, ".net.mptcp.mptcp_debug", "1");
  stack.SysctlSet (serverNodes, ".net.mptcp.mptcp_debug", "1");

#if 1
  LinuxStackHelper::SysctlGet (serverNodes.Get (0), NanoSeconds (0),
                               ".net.ipv4.tcp_available_congestion_control", &PrintTcpFlags);
  LinuxStackHelper::SysctlGet (serverNodes.Get (0), NanoSeconds (0),
                               ".net.ipv4.tcp_rmem", &PrintTcpFlags);
  LinuxStackHelper::SysctlGet (serverNodes.Get (0), NanoSeconds (0),
                               ".net.ipv4.tcp_wmem", &PrintTcpFlags);
  LinuxStackHelper::SysctlGet (serverNodes.Get (0), NanoSeconds (0),
                               ".net.core.rmem_max", &PrintTcpFlags);
  LinuxStackHelper::SysctlGet (serverNodes.Get (0), NanoSeconds (0),
                               ".net.core.wmem_max", &PrintTcpFlags);
  LinuxStackHelper::SysctlGet (serverNodes.Get (0), Seconds (1),
                               ".net.ipv4.tcp_available_congestion_control", &PrintTcpFlags);
  LinuxStackHelper::SysctlGet (serverNodes.Get (0), Seconds (1),
                               ".net.ipv4.tcp_rmem", &PrintTcpFlags);
  LinuxStackHelper::SysctlGet (serverNodes.Get (0), Seconds (1),
                               ".net.ipv4.tcp_wmem", &PrintTcpFlags);
  LinuxStackHelper::SysctlGet (serverNodes.Get (0), Seconds (1),
                               ".net.core.rmem_max", &PrintTcpFlags);
  LinuxStackHelper::SysctlGet (serverNodes.Get (0), Seconds (1),
                               ".net.core.wmem_max", &PrintTcpFlags);
#endif

  DceApplicationHelper dce;
  ApplicationContainer apps;

  dce.SetStackSize (1 << 20);

  // Launch iperf client on node 0
  dce.SetBinary ("iperf");
  dce.ResetArguments ();
  dce.ResetEnvironment ();
  dce.AddArgument ("-c");
  dce.AddArgument ("10.2.0.1");
  dce.ParseArguments ("-y C");
  dce.AddArgument ("-i");
  dce.AddArgument ("1");
  dce.AddArgument ("--time");
  dce.AddArgument ("40");

  apps = dce.Install (staNodes.Get (0));
  apps.Start (Seconds (5.0));
  //  apps.Stop (Seconds (15));

  // Launch iperf server on node 1
  dce.SetBinary ("iperf");
  dce.ResetArguments ();
  dce.ResetEnvironment ();
  dce.AddArgument ("-s");
  dce.AddArgument ("-P");
  dce.AddArgument ("1");

  apps = dce.Install (serverNodes.Get (0));
  apps.Start (Seconds (4));

  p2p.EnablePcapAll ("mptcp-lte-wifi", false);
  phy.EnablePcapAll ("mptcp-lte-wifi", false);
  //  lteHelper->EnableTraces ();

  // Output config store to txt format
  Config::SetDefault ("ns3::ConfigStore::Filename", StringValue ("output-attributes.txt"));
  Config::SetDefault ("ns3::ConfigStore::FileFormat", StringValue ("RawText"));
  Config::SetDefault ("ns3::ConfigStore::Mode", StringValue ("Save"));
  ConfigStore outputConfig2;
  outputConfig2.ConfigureDefaults ();
  outputConfig2.ConfigureAttributes ();

  AnimationInterface anim("wtf.xml");

  Simulator::Stop (Seconds (15));
  Simulator::Run ();
  Simulator::Destroy ();

  return 0;
}
