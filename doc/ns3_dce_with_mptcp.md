# NS3-DCE with MPTCP

## Prerequisites

- For Ubuntu 14.04 LTS, upgrade gcc to gcc-4.9 as described in [1][upgrade-gcc].
- Install prerequisites as described in [2][prerequisites].
- Some other packages may be needed:

		cvs, bzr, urar, p7zip-full, libfs-dev, libdb-dev, libpcap-dev

## Build a libos library

Follow the instructions in [3][mptcp-with-libos] and [4][libos-howto].

- Download the source code

		git clone https://github.com/libos-nuse/net-next-nuse.git
		git checkout mptcp_trunk_libos

- Generate and modify default config file

		make defconfig ARCH=lib
		make menuconfig ARCH=lib
		make library ARCH=lib
You can check `Network support -> Network options -> MPTCP: advanced scheduler control -> MPTCP Round-Robin` for potential usage.
- Obtain network simulator environment

		make testbin -C tools/testing/libos
		make test ARCH=lib
If failed, execute `bake.py download -vvv` manually to see what happened. The download url for ccnx is invalid, which can be ignored or be fixed by providing valid url and corresponding folder name in bakefile.xml.

[upgrade-gcc]: https://askubuntu.com/questions/466651/how-do-i-use-the-latest-gcc-on-ubuntu
[prerequisites]: https://www.nsnam.org/wiki/Installation
[mptcp-with-libos]: https://github.com/libos-nuse/net-next-nuse/wiki/Linux-mptcp-with-libos
[libos-howto]: https://github.com/libos-nuse/net-next-nuse/blob/nuse/Documentation/virtual/libos-howto.txt

## Run examples
- Under `ns-3-dce` folder:

		./waf --run dce-mptcp-lte-wifi
- Using NetAnim:
	- Ensure that your wscript includes the "netanim" module.
	- Include the header file `ns3/netanim-module.h`.
	- Add the statement `AnimationInterface anim("animation.xml");` before `Simulator::Run();`.
	- Run the code and open the xml file with NetAnim.

https://www.nsnam.org/wiki/NetAnim_3.105


https://www.nsnam.org/docs/dce/manual-quagga/html/getting-started.html

make BAKE_HOME to be libos/bake

go to `buildtop`

sudo apt-get install indent

bake.py download
bake.py build

Ptr<MobilityModel> mob = staNodes.Get(i)->GetObject<MobilityModel>();
Vector pos = mob->GetPosition ();
std::cout  << i << ":" << "  " << pos.x << ", " << pos.y << std::endl;