from iface_mgr import IfaceMgr
import threading
import os
import sys
import time

PATH_FOR_TEST = './test_single_ap'
PATH_FOR_RESULT = './test_single_ap/second_batch'

# AP = 'C0:61:18:FC:8B:2C'.lower()
AP = '18:D2:76:A0:E2:64'.lower()


class StatsThread(threading.Thread):
	def __init__(self, ifmgr1, ifmgr2, testname):
		threading.Thread.__init__(self)
		self.ifmgr1 = ifmgr1
		self.ifmgr2 = ifmgr2
		self.testname = testname
		self.cont = True

	def run(self):
		while self.cont:
			ap1 = self.ifmgr1.update_current_ap()
			if ap1 is not None:
				ts = str(time.time())
				stats1 = ts + '\t' + ap1.bssid + '\t' + ap1.signal
				os.system('echo {} >> {}/{}w1.stats'.format(stats1, PATH_FOR_RESULT, self.testname))

			ap2 = self.ifmgr2.update_current_ap()
			if ap2 is not None:
				ts = str(time.time())
				stats2 = ts + '\t' + ap2.bssid + '\t' + ap2.signal
				os.system('echo {} >> {}/{}w2.stats'.format(stats2, PATH_FOR_RESULT, self.testname))

	def stop(self):
		self.cont = False


def test_single_ap_two_nics(testname):
	if not os.path.exists(PATH_FOR_TEST):
		os.mkdir(PATH_FOR_TEST)
	if not os.path.exists(PATH_FOR_RESULT):
		os.mkdir(PATH_FOR_RESULT)

	os.system('xterm -hold -e speedometer -t wlan5 &')
	os.system('xterm -hold -e speedometer -t wlan6 &')

	if5 = IfaceMgr('wlan5')
	if5.connect_by_bssid(AP)

	if6 = IfaceMgr('wlan6')
	if6.connect_by_bssid(AP)

	time.sleep(1)

	os.system('xterm -hold -e iperf -c 114.212.80.16 -p 10086 -t 3600 &')
	os.system('xterm -hold -e tcpdump -i wlan5 -w {}/{}w1.pcap &'.format(PATH_FOR_RESULT, testname))
	os.system('xterm -hold -e tcpdump -i wlan6 -w {}/{}w2.pcap &'.format(PATH_FOR_RESULT, testname))

	thread = StatsThread(if5, if6, testname)
	thread.start()

	while True:
		c = raw_input('continue?[y/n]')
		if c.lower() == 'n':
			thread.stop()
			break

	os.system('killall tcpdump')
	os.system('killall iperf')


if __name__ == '__main__':
	if len(sys.argv) < 2:
		print('Usage: {} testname'.format(sys.argv[0]))
		sys.exit(1)

	test_single_ap_two_nics(sys.argv[1])
