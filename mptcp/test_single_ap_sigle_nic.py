from iface_mgr import IfaceMgr
import threading
import os
import sys
import time

PATH_FOR_TEST = './test_single_ap'
PATH_FOR_RESULT = './test_single_ap/first_batch'

# AP = 'C0:61:18:FC:8B:2C'.lower()

AP = '18:D2:76:A0:E2:64'.lower()


class StatsThread(threading.Thread):
	def __init__(self, ifmgr, testname):
		threading.Thread.__init__(self)
		self.ifmgr = ifmgr
		self.testname = testname
		self.cont = True

	def run(self):
		while self.cont:
			ap = self.ifmgr.update_current_ap()
			if ap is not None:
				ts = str(time.time())
				stats = ts + '\t' + ap.bssid + '\t' + ap.signal
				os.system('echo {} >> {}/{}.stats'.format(stats, PATH_FOR_RESULT, self.testname))

	def stop(self):
		self.cont = False


def test_single_ap_single_nic(testname):
	if not os.path.exists(PATH_FOR_TEST):
		os.mkdir(PATH_FOR_TEST)

	if not os.path.exists(PATH_FOR_RESULT):
		os.mkdir(PATH_FOR_RESULT)

	os.system('xterm -hold -e speedometer -t wlan5 &')

	if5 = IfaceMgr('wlan5')
	if5.connect_by_bssid(AP)

	os.system('xterm -hold -e iperf -c 114.212.80.16 -p 10086 -t 3600 &')
	os.system('xterm -hold -e tcpdump -i wlan5 -w {}/{}.pcap &'.format(PATH_FOR_RESULT, testname))

	thread = StatsThread(if5, testname)
	thread.start()

	while True:
		c = raw_input('continue?[y/n]')
		if c.lower() == 'n':
			thread.stop()
			break

	os.system('killall tcpdump')
	os.system('killall iperf')


if __name__ == '__main__':
	if len(sys.argv) < 2:
		print('Usage: {} testname'.format(sys.argv[0]))
		sys.exit(1)

	test_single_ap_single_nic(sys.argv[1])
