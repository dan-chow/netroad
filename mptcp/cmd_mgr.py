from subprocess import Popen, PIPE


# TODO shell pipelining
def run_cmd(cmd):
	proc = Popen(cmd.split(), shell=False, stdout=PIPE, stderr=PIPE)
	out, err = proc.communicate()
	if err.strip() != '':
		print(err)
	return out.strip()
