from cmd_mgr import run_cmd
import time
import os
import re


class APInfo:
	def __init__(self, ap_str):
		self.ap_str = ap_str
		tokens = ap_str.split('\t')
		self.bssid = tokens[0].lower()
		self.frequency = tokens[1]
		self.signal = tokens[2]
		self.flags = tokens[3]
		self.essid = tokens[4]

	def __str__(self):
		return self.ap_str


class IfaceMgr:
	MAX_TRY = 20
	DHCP_LEASE_FILE = '/var/lib/dhcp/dhclient.leases'
	# REG_EXPR_IP = '"([0-9]{1,3}.){3}[0-9]{1,3}"'

	def __init__(self, iface):
		self.iface = iface
		self.current_ap = None

		nm_status = run_cmd('service network-manager status')
		if 'running' in nm_status:
			run_cmd('service network-manager stop')
			pid = run_cmd('cat /run/sendsigs.omit.d/wpasupplicant.pid')
			run_cmd('kill {}'.format(pid))
			time.sleep(1)

		run_cmd('wpa_cli -i {} terminate'.format(self.iface))
		time.sleep(1)
		run_cmd('wpa_supplicant -B -i {} -C /var/run/wpa_supplicant'.format(self.iface))
		time.sleep(1)

	def disconnect_current(self):
		run_cmd('ip addr flush dev {}'.format(self.iface))
		time.sleep(1)

		networks = run_cmd('wpa_cli -i {} list_network'.format(self.iface)).split('\n')[1:]
		for net in networks:
			if net.rstrip() == '':
				break
			nw_id = net.split()[0]
			run_cmd('wpa_cli -i {} remove_network {}'.format(self.iface, nw_id))
			print('removing network:{}'.format(nw_id))

		self.current_ap = None

	def scan_by_bssid(self, bssid):
		bssid.lower()
		for i in range(self.MAX_TRY):
			scan_status = run_cmd('wpa_cli -i {} scan'.format(self.iface))
			time.sleep(1)
			print('scan {}'.format('OK' if 'OK' in scan_status else 'FAILED'))

			scan_result = run_cmd('wpa_cli -i {} scan_result'.format(self.iface))
			target_ap = re.search(r'{}.*'.format(bssid), scan_result)
			if target_ap is None:
				print('ap {} not found'.format(bssid))
			else:
				print(target_ap.group())
				return APInfo(target_ap.group())
		return None

	def associate_by_bssid(self, bssid):
		net_id = run_cmd('wpa_cli -i {} add_network'.format(self.iface))

		for i in range(self.MAX_TRY):
			run_cmd('iwconfig {} ap {}'.format(self.iface, bssid))

			set_bssid_status = run_cmd('wpa_cli -i {} set_network {} bssid {}'.format(self.iface, net_id, bssid))
			print('set bssid {}'.format('OK' if 'OK' in set_bssid_status else 'FAILED'))

			set_key_status = run_cmd('wpa_cli -i {} set_network {} key_mgmt NONE'.format(self.iface, net_id))
			print('set key {}'.format('OK' if 'OK' in set_key_status else 'FAILED'))

			select_status = run_cmd('wpa_cli -i {} select_network {}'.format(self.iface, net_id))
			time.sleep(1)
			print('select {}'.format('OK' if 'OK' in select_status else 'FAILED'))

			if self.is_associated(bssid):
				return True
		return False

	def is_associated(self, bssid):
		status = run_cmd('wpa_cli -i {} status'.format(self.iface))
		print(status)
		return 'wpa_state=COMPLETED' in status and bssid in status

	def dhcp(self):
		if self.iface in run_cmd('ip rule'.format(self.iface)):
			run_cmd('ip rule del lookup {}'.format(self.iface))
		ip_route = run_cmd('ip route')

		global_default = re.search(r'default.*', ip_route)
		if global_default is not None:
			if self.iface in global_default.group():
				run_cmd('ip route del default')

		run_cmd('ip route flush table {}'.format(self.iface))
		run_cmd('dhclient {}'.format(self.iface))

	def has_ip(self):
		status = run_cmd("wpa_cli -i {} status".format(self.iface))
		return 'ip_address=' in status

	def update_routing(self, gateway, ip):

		if self.iface in run_cmd('ip rule'.format(self.iface)):
			run_cmd('ip rule del lookup {}'.format(self.iface))
		run_cmd('ip rule add from {} table {}'.format(ip, self.iface))

		run_cmd('ip route flush table {}'.format(self.iface))
		# TODO infer network address from ip and mask
		run_cmd('ip route add 192.168.8.0/24 dev {} scope link table {}'.format(self.iface, self.iface))
		run_cmd('ip route add default via {} dev {} table {}'.format(gateway, self.iface, self.iface))

		ip_route = run_cmd('ip route')
		global_default = re.search(r'default.*', ip_route)

		if global_default is None:
			run_cmd('ip route add default scope global nexthop via {} dev {}'.format(gateway, self.iface))
		else:
			run_cmd('ip route del default')
			run_cmd('ip route add default scope global nexthop via {} dev {}'.format(gateway, self.iface))

	def connect_by_bssid(self, bssid):
		bssid = bssid.lower()

		self.disconnect_current()

		print('connecting {} to {} ...'.format(self.iface, bssid))

		ap = self.scan_by_bssid(bssid)
		if ap is None:
			return

		self.associate_by_bssid(bssid)
		print('associate {}'.format('OK' if self.is_associated(bssid) else 'FAILED'))

		run_cmd('rm {}'.format(self.DHCP_LEASE_FILE))
		self.dhcp()
		while not self.has_ip():
			time.sleep(1)

		while not os.path.exists(self.DHCP_LEASE_FILE):
			time.sleep(1)

		print('dhcp {}'.format('OK' if self.has_ip() else 'FAILED'))

		if self.has_ip():
			lease = run_cmd('cat {}'.format(self.DHCP_LEASE_FILE))
			if self.iface not in lease:
				time.sleep(1)
				lease = run_cmd('cat {}'.format(self.DHCP_LEASE_FILE))

			print(lease)
			gw = re.search(r'option routers.*', lease).group()
			gw = re.search(r'(\d{1,3}\.){3}\d{1,3}', gw).group()

			ip = re.search(r'fixed-address.*', lease).group()
			ip = re.search(r'(\d{1,3}\.){3}\d{1,3}', ip).group()
			
			# TODO shell pipelining
			# gw = run_cmd('cat {} | grep routers | egrep -o {}'.format(self.DHCP_LEASE_FILE, self.REG_EXPR_IP))
			# ip = run_cmd('cat {} | grep fixed-address | egrep -o {}'.format(self.DHCP_LEASE_FILE, self.REG_EXPR_IP))

			self.update_routing(gw, ip)
			self.current_ap = ap

	def get_ap_by_bssid(self, bssid):
		bssid = bssid.lower()

		scan_status = run_cmd('wpa_cli -i {} scan'.format(self.iface))
		time.sleep(1)
		if 'OK' not in scan_status:
			return None

		scan_result = run_cmd('wpa_cli -i {} scan_result'.format(self.iface))
		aps = scan_result.split('\n')[1:]

		for ap in aps:
			if bssid in ap:
				return APInfo(ap)

		return None

	def update_current_ap(self):
		ap = self.scan_by_bssid(self.current_ap.bssid)

		if ap is not None:
			self.current_ap = ap

		return self.current_ap
