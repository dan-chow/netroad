from iface_mgr import IfaceMgr
import threading
import os
import sys
import time

# TODO parameter to be refined
HANDOVER_DIFF_THREASHOLD = 5

PATH_FOR_TEST = './test_two_aps'
PATH_FOR_RESULT = './test_two_aps/first_batch'

FIRST_AP = 'C0:61:18:FC:8B:2C'.lower()
SECOND_AP = 'C0:61:18:FD:81:90'.lower()


class StatsAndHandoverThread(threading.Thread):
	def __init__(self, ifmgr, testname):
		threading.Thread.__init__(self)
		self.ifmgr = ifmgr
		self.testname = testname
		self.cont = True

	def run(self):
		while self.cont:
			time.sleep(0.5)
			ap = self.ifmgr.update_current_ap()
			if ap is not None:
				ts = str(time.time())
				stats = ts + '\t' + ap.bssid + '\t' + ap.signal
				os.system('echo {} >> {}/{}.stats'.format(stats, PATH_FOR_RESULT, self.testname))
			elif ap.bssid != SECOND_AP:
				ap = self.ifmgr.get_ap_by_bssid(SECOND_AP)
				if ap is not None:
					self.ifmgr.connect_by_bssid(SECOND_AP)

			time.sleep(1)

			if self.ifmgr.current_ap.bssid != SECOND_AP:
				ap = self.ifmgr.get_ap_by_bssid(SECOND_AP)

				if ap is not None:
					print('ap1:{},ap2{}'.format(self.ifmgr.current_ap.signal, ap.signal))
					if float(ap.signal) - float(self.ifmgr.current_ap.signal) > HANDOVER_DIFF_THREASHOLD:
						self.ifmgr.connect_by_bssid(SECOND_AP)

	def stop(self):
		self.cont = False


def test_two_aps_single_nic(testname):
	if not os.path.exists(PATH_FOR_TEST):
		os.mkdir(PATH_FOR_TEST)
	if not os.path.exists(PATH_FOR_RESULT):
		os.mkdir(PATH_FOR_RESULT)

	os.system('xterm -hold -e speedometer -t wlan5 &')
	if5 = IfaceMgr('wlan5')
	if5.connect_by_bssid(FIRST_AP)

	os.system('xterm -hold -e iperf -c 114.212.85.193 -t 3600 &')
	os.system('xterm -hold -e tcpdump -i wlan5 -w {}/{}.pcap &'.format(PATH_FOR_RESULT, testname))

	thread = StatsAndHandoverThread(if5, testname)
	thread.start()

	while True:
		c = raw_input('continue?[y/n]')
		if c.lower() == 'n':
			thread.stop()
			break

	os.system('killall tcpdump')
	os.system('killall iperf')

if __name__ == '__main__':
	if len(sys.argv) < 2:
		print('Usage: {} testname'.format(sys.argv[0]))
		sys.exit(1)

	test_two_aps_single_nic(sys.argv[1])
