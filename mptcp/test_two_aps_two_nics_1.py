from iface_mgr import IfaceMgr
import threading
import os
import sys
import time

# TODO parameter to be refined
HANDOVER_DIFF_THREASHOLD = 5

PATH_FOR_TEST = './test_two_aps'
PATH_FOR_RESULT = './test_two_aps/second_batch'

# FIRST_AP = 'C0:61:18:FC:8B:2C'.lower()
# SECOND_AP = 'C0:61:18:FD:81:90'.lower()

FIRST_AP = '18:D2:76:A0:E2:64'.lower()
SECOND_AP = '18:D2:76:A0:DD:58'.lower()


class StatsAndHandoverThread(threading.Thread):
	def __init__(self, ifmgr1, ifmgr2, testname):
		threading.Thread.__init__(self)
		self.ifmgr1 = ifmgr1
		self.ifmgr2 = ifmgr2
		self.testname = testname
		self.cont = True

	def run(self):
		while self.cont:
			time.sleep(0.5)

			if self.ifmgr1.current_ap is not None:
				ap = self.ifmgr1.update_current_ap()
				if ap is not None:
					ts = str(time.time())
					stats = ts + '\t' + ap.bssid + '\t' + ap.signal
					os.system('echo {} >> {}/{}w1.stats'.format(stats, PATH_FOR_RESULT, self.testname))
			else:
				ap = self.ifmgr2.update_current_ap()
				if ap is not None:
					ts = str(time.time())
					stats = ts + '\t' + ap.bssid + '\t' + ap.signal
					os.system('echo {} >> {}/{}w2.stats'.format(stats, PATH_FOR_RESULT, self.testname))
			if self.ifmgr1.current_ap is not None:
				ap = self.ifmgr2.get_ap_by_bssid(SECOND_AP)
				if ap is not None and float(ap.signal) - float(self.ifmgr1.current_ap.signal) > HANDOVER_DIFF_THREASHOLD:
					self.ifmgr2.connect_by_bssid(SECOND_AP)
					self.ifmgr1.disconnect_current()

	def stop(self):
		self.cont = False


# only one nic work
def test_two_aps_two_nics_1(testname):
	if not os.path.exists(PATH_FOR_TEST):
		os.mkdir(PATH_FOR_TEST)
	if not os.path.exists(PATH_FOR_RESULT):
		os.mkdir(PATH_FOR_RESULT)

	os.system('xterm -hold -e speedometer -t wlan5 &')
	os.system('xterm -hold -e speedometer -t wlan6 &')

	if5 = IfaceMgr('wlan5')
	if5.connect_by_bssid(FIRST_AP)
	if6 = IfaceMgr('wlan6')

	os.system('xterm -hold -e iperf -c 114.212.80.16 -p 10086 -t 3600 &')
	os.system('xterm -hold -e tcpdump -i wlan5 -w {}/{}w1.pcap &'.format(PATH_FOR_RESULT, testname))
	os.system('xterm -hold -e tcpdump -i wlan6 -w {}/{}w2.pcap &'.format(PATH_FOR_RESULT, testname))

	thread = StatsAndHandoverThread(if5, if6, testname)
	thread.start()

	while True:
		c = raw_input('continue?[y/n]')
		if c.lower() == 'n':
			thread.stop()
			break

	os.system('killall tcpdump')
	os.system('killall iperf')

if __name__ == '__main__':
	if len(sys.argv) < 2:
		print('Usage: {} testname'.format(sys.argv[0]))
		sys.exit(1)

	test_two_aps_two_nics_1(sys.argv[1])
