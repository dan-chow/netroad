#! /bin/bash

sudo ip rule add from 192.168.1.171 table 1
sudo ip rule add from 192.168.1.238 table 2

sudo ip route add 192.168.1.0/24 dev wlan0 scope link table 1
sudo ip route del default table 1
sudo ip route add default via 192.168.1.9 dev wlan0 table 1

sudo ip route add 192.168.1.0/24 dev wlan5 scope link table 2
sudo ip route del default table 2
sudo ip route add default via 192.168.1.9 dev wlan5 table 2

# sudo ip route del default
# sudo ip route add default scope global nexthop via 192.168.1.9 dev wlan5
