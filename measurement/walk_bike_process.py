from util_stats import StatsProcessor
from util_pcap import PcapProcessor
import matplotlib.pyplot as plt
import sys

if __name__ == '__main__':

    test_cnt = 1
    res_stats1 = []
    res_stats2 = []

    
    for i in range(test_cnt):
        stats_file1 = 'walk6.bak.stats'
        stats_proc1 = StatsProcessor(stats_file1)
        res_stats1.append(stats_proc1.get_stats(2))

        stats_file2 = 'bike04.bak.stats'
        stats_proc2 = StatsProcessor(stats_file2)
        res_stats2.append(stats_proc2.get_stats(2))

    mac1 = res_stats1[0].keys()[0]
    mac2 = res_stats2[0].keys()[0]

    for i in range(test_cnt):
        plt.plot(res_stats1[i][mac1]['timestamp'], res_stats1[i][mac1]['utilization'])
        plt.plot(res_stats2[i][mac2]['timestamp'], res_stats2[i][mac2]['utilization'])
    plt.xlabel('time')
    plt.ylabel('utilization')
    plt.show()

    for i in range(test_cnt):
        plt.plot(res_stats1[i][mac1]['timestamp'], res_stats1[i][mac1]['signal'])
        plt.plot(res_stats2[i][mac2]['timestamp'], res_stats2[i][mac2]['signal'])
    plt.xlabel('time')
    plt.ylabel('signal level (dBm)')
    plt.show()

    for i in range(test_cnt):
        plt.plot(res_stats1[i][mac1]['timestamp'], res_stats1[i][mac1]['tx_rate'])
        plt.plot(res_stats2[i][mac2]['timestamp'], res_stats2[i][mac2]['tx_rate'])
    plt.xlabel('time')
    plt.ylabel('tx rate (Mbps)')
    plt.show()

    res_pcap1 = []
    res_pcap2 = []
    for i in range(test_cnt):
        pcap_file1 = 'walk6.pcap'
        pcap_proc1 = PcapProcessor(pcap_file1)
        res_pcap1.append(pcap_proc1.get_stats(2))

        pcap_file2 = 'bike04.pcap'
        pcap_proc2 = PcapProcessor(pcap_file2)
        res_pcap2.append(pcap_proc2.get_stats(2))

    for i in range(test_cnt):
        plt.plot(res_pcap1[i]['timestamp'], res_pcap1[i]['rx_rate'])
        plt.plot(res_pcap2[i]['timestamp'], res_pcap2[i]['rx_rate'])
    plt.xlabel('time')
    plt.ylabel('rx rate (Kbps)')
    plt.show()
