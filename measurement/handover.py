from util_ap import APManager
import sys

if __name__ == '__main__':
    if(len(sys.argv) <= 2):
        print 'Usage: {} iface ap_mac'.format(sys.argv[0])
        print 'sudo python {} wlan5 "C0:61:18:FD:81:90"'.format(sys.argv[0])
        print 'sudo python {} wlan5 "C0:61:18:FC:8B:2C"'.format(sys.argv[0])
        
        sys.exit(1)

    apmgr = APManager(sys.argv[1])
    apmgr.associate_to(sys.argv[2])
