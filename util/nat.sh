#!/usr/bin/env bash

# 114.212.80.16

sudo sysctl net.ipv4.ip_forward=1
sudo iptables -t nat -A PREROUTING -p tcp --dport 10086 -j DNAT --to-destination 114.212.85.193:5001
sudo iptables -t nat -A POSTROUTING -j MASQUERADE