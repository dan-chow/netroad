#! /bin/bash

contains(){
	string=$1
	substring=$2

	if test "${string#*$substring}" != "$string"; then
        	echo true
	else
	        echo false
	fi
}

if [ $# -ne 1 ]; then
	echo "usage: $0 filename"
	exit 1
fi

result="["
is_first_entry=true
while read line; do
    if [ $(contains "$line" "Survey") = true ]; then
        if [ $is_first_entry = true ]; then
			result="$result{"
		    is_first_entry=false
	    else
			result="$result},{"
        fi
    fi

    if [ $(contains "$line" "frequency") = true ]; then
        frequency=$(echo $line | egrep -o '[0-9]+.*MHz' | egrep -o '[0-9]+')
	result="$result\"frequency\":$frequency"
    fi

    if [ $(contains "$line" "noise") = true ]; then
        noise=$(echo $line | egrep -o '[+-]?[0-9]+.*dBm' | egrep -o '[+-]?[0-9]+')
	result="$result,\"noise\":$noise"
    fi

    if [ $(contains "$line" "channel active time") = true ]; then
        active_time=$(echo $line | egrep -o '[0-9]+.*ms' | egrep -o '[0-9]+')
	result="$result,\"channel active time\":$active_time"
    fi

    if [ $(contains "$line" "channel busy time") = true ]; then
        busy_time=$(echo $line | egrep -o '[0-9]+.*ms' | egrep -o '[0-9]+')
	result="$result,\"channel busy time\":$busy_time"
    fi

    if [ $(contains "$line" "channel receive time") = true ]; then
        receive_time=$(echo $line | egrep -o '[0-9]+.*ms' | egrep -o '[0-9]+')
	result="$result,\"channel receive time\":$receive_time"
    fi

    if [ $(contains "$line" "channel transmit time") = true ]; then
        transmit_time=$(echo $line | egrep -o '[0-9]+.*ms' | egrep -o '[0-9]+')
	result="$result,\"channel transmit time\":$transmit_time"
    fi

done < $1

result="$result}]"

echo $result
