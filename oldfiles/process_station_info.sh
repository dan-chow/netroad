#! /bin/bash

n=0
is_first_entry=true
printf "["
while read line;
do
    if [[ $line == *"Station"* ]]; then
	if [ $is_first_entry = true ]; then
		printf "{"
		is_first_entry=false
	else
		printf "},{"
	fi
        mac=$(echo $line | egrep -o '([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})')
	printf "\"mac\":\"%s\"," $mac
        ((n++))
    fi
    if [[ $line == *"signal:"* ]]; then
        signal=$(echo $line | egrep -o 'signal.*\[' | egrep -o '(+|-)*[0-9]+')
	printf "\"signal\":%d" $signal
    fi

done < station.dump

printf "}]"
