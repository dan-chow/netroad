from pcapfile import savefile
from pcapfile.protocols.linklayer import ethernet
from pcapfile.protocols.network import ip
from pcapfile.protocols.transport import tcp
import binascii
import sys

def process_pcap(input_file, time_interval, client_ip, server_ip):
    inputcap = open(input_file, 'rb')
    capfile = savefile.load_savefile(inputcap, verbose=False)
    pkts = capfile.packets

    client_seq = set()
    server_ack = set()
    tx_bytes = 0
    rx_bytes = 0
    tx_pkts = 0
    rx_pkts = 0
    tx_loss = 0
    rx_loss = 0
    start_ts = pkts[0].timestamp

    for pkt in pkts:
        print pkt.timestamp
        if (pkt.timestamp - start_ts) < time_interval:
            eth_frame = ethernet.Ethernet(pkt.raw())

            if eth_frame.type != 0x0800:
                continue

            ip_pkt = ip.IP(binascii.unhexlify(eth_frame.payload))
            tcp_seg = tcp.TCP(binascii.unhexlify(ip_pkt.payload))
            seq = tcp_seg.seqnum
            ack = tcp_seg.acknum

            print "============"
            print eth_frame.src
            print eth_frame.dst
            print seq
            print ack
            print "============"
            
            if ip_pkt.src == client_ip and ip_pkt.dst == server_ip:
                tx_bytes += len(pkt.raw())
                tx_pkts += 1

                if seq not in client_seq:
                    client_seq.add(seq)
                else:
                    tx_loss += 1

                continue
            if ip_pkt.src == server_ip and ip_pkt.dst == client_ip:
                rx_bytes += len(pkt.raw())
                rx_pkts += 1

                if ack not in server_ack:
                    server_ack.add(ack)
                else:
                    rx_loss += 1
	else:
            rx_loss_rate = 1.0 * tx_loss / tx_pkts
            tx_loss_rate = 1.0 * rx_loss / rx_pkts
            tx_rate = 8.0 * tx_bytes / time_interval / 1024 / 1024
            rx_rate = 8.0 * rx_bytes / time_interval / 1024 / 1024

            print "tx_rate:%s tx_loss_rate:%s" % (tx_rate, tx_loss_rate)
            print "rx_rate:%s rx_loss_rate:%s" % (rx_rate, rx_loss_rate)


            client_seq.clear()
            server_ack.clear()
            tx_bytes = 0
            rx_bytes = 0
            tx_pkts = 0
            rx_pkts = 0
            tx_loss = 0
            tx_loss = 0
	    start_ts = pkt.timestamp


if __name__ == "__main__":

    if len(sys.argv) < 2:
        print "Usage: %s inputfile" % (sys.argv[0])
        sys.exit(-1)

    inputfile = sys.argv[1]

    process_pcap(inputfile, 10, "192.168.1.174", "114.212.85.193")
