#! /usr/bin/env python

from subprocess import Popen, PIPE
import re
import sys
import time
import numpy as np


def get_channel_utilization(ifname):
	cmd = "iw dev %s survey dump" % ifname

	proc = Popen(cmd, stdout=PIPE, shell=True)
	(out, err) = proc.communicate()

	busy_time_str = re.search(r'busy time:(.)*ms', out).group()
	busy_time_str = re.search(r'\d+', busy_time_str).group()

	active_time_str = re.search(r'active time:(.)*ms', out).group()
	active_time_str = re.search(r'\d+', active_time_str).group()

	return float(busy_time_str) / float(active_time_str)


if __name__ == "__main__":

	if len(sys.argv) < 2:
		print
		"Usage: %s ifname" % sys.argv[0]
		sys.exit(-1)

	ifname = sys.argv[1]

	cnt = 30
	results = [None] * cnt

	for i in range(cnt):
		time.sleep(0.05)
		results[i] = get_channel_utilization(ifname)

	mean = np.mean(results)
	print
	'channel_utilization=%s' % mean
