#! /usr/bin/env python

import os
import sys
import numpy as np
from scapy.all import *


def get_beacon_time(in_ap_mac, out_beacon_times):
	def packet_handler(pkt):
		if pkt.haslayer(Dot11) and pkt.type == 0 and pkt.subtype == 8 and pkt.addr2.lower() == in_ap_mac.lower():
			out_beacon_times.append(pkt.time)

	return packet_handler


def get_beacon_interval(ifname, ap_mac):
	beacon_times = []

	os.system("sudo ifconfig %s down" % ifname)
	os.system("sudo iwconfig %s mode monitor" % ifname)
	os.system("sudo ifconfig %s up" % ifname)
	
	sniff(iface=ifname, prn=get_beacon_time(ap_mac, beacon_times), timeout=10)

	beacon_intervals = []
	for i in range(len(beacon_times) - 1):
		beacon_intervals.append(beacon_times[i + 1] - beacon_times[i])

	os.system("sudo ifconfig %s down" % ifname)
	os.system("sudo iwconfig %s mode managed" % ifname)
	os.system("sudo ifconfig %s up" % ifname)

	return (len(beacon_times), np.mean(beacon_intervals))


if __name__ == "__main__":

	if len(sys.argv) < 3:
		print
		"Usage: %s ifname ap_mac" % sys.argv[0]
		sys.exit(-1)

	ifname = sys.argv[1]
	ap_mac = sys.argv[2]
	(beacon_cnt, beacon_interval_mean) = get_beacon_interval(ifname, ap_mac)

	print
	'beacon_cnt=%s,beacon_interval=%s' % (beacon_cnt, beacon_interval_mean)
