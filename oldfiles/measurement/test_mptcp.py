from util_threads import WgetThread, TcpdumpThread
from util_ap import APManager
import os
import sys

if __name__ == '__main__':

    if(len(sys.argv) <= 3):
        print('Usage: {} iface1 iface2 testname'.format(sys.argv[0]))
        sys.exit(1)

    if not os.path.exists('./result_mptcp'):
        os.mkdir('./result_mptcp')

    iface1 = sys.argv[1]
    iface2 = sys.argv[2]
    testname = sys.argv[3]

    file_dl = './result_mptcp/{}.dl'.format(testname)
    file_pcap1 = './result_mptcp/{}_if1.pcap'.format(testname)
    file_pcap2 = './result_mptcp/{}_if2.pcap'.format(testname)

    if(os.path.exists(file_dl) or os.path.exists(file_pcap1) or os.path.exists(file_pcap2)):
        print('file exists!')
        exit(2)

    url = 'http://multipath-tcp.org/snapshots/mptcp_2016_04_11.tar.gz'
    t_wget = WgetThread(url, file_dl)
    t_wget.start()

    t_tcpdump1 = TcpdumpThread(iface1, file_pcap1)
    t_tcpdump1.start()
    
    t_tcpdump2 = TcpdumpThread(iface2, file_pcap2)
    t_tcpdump2.start()
    
    while(True):
        cmd = raw_input('press t to terminate:')
        if(cmd.lower() == 't'):
            t_tcpdump1.stop()
            t_tcpdump2.stop()
            t_wget.stop()

            os._exit(0)
