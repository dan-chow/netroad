from util_ap import APInfo, APManager
from subprocess import Popen, PIPE
import threading
import time
import os

class WgetThread(threading.Thread):
    def __init__(self, url, outfile):
        threading.Thread.__init__(self)
        self.proc = None
        self.url = url
        self.outfile = outfile
    def run(self):
        cmd = 'wget {} -O {}'.format(self.url, self.outfile)
        self.proc = Popen(cmd.split(), shell=False, stdout=PIPE, stderr=PIPE)
        (out, err) = self.proc.communicate()
    def stop(self):
        if(self.proc is not None):
            self.proc.terminate()
            self.proc = None
        if(os.path.exists(self.outfile)):
            os.remove(self.outfile)

class TcpdumpThread(threading.Thread):
    def __init__(self, iface, outfile):
        threading.Thread.__init__(self)
        self.proc = None
        self.iface = iface
        self.outfile = outfile
    def run(self):
        cmd = 'tcpdump -e -n -i {} -w {}'.format(self.iface, self.outfile)
        self.proc = Popen(cmd.split(), shell=False, stdout=PIPE, stderr=PIPE)
        (out, err) = self.proc.communicate()
    def stop(self):
        if(self.proc is not None):
            self.proc.terminate()
            self.proc = None

class StatsThread(threading.Thread):
    def __init__(self, iface, outfile):
        threading.Thread.__init__(self)
        self.iface = iface
        self.outfile = outfile
        self.openfile = None
        self.cont = True
    def run(self):
        self.openfile = open(self.outfile, 'w')
        while(self.cont):
            ts = time.time()

            apmgr = APManager(self.iface)
            ap = apmgr.get_current_ap()
            if(ap is None):
                continue
            res_channel = apmgr.get_channel_stats()
            res_sig_rate = apmgr.get_signal_and_rate_stats()
            
            data_str = '{}\t{}\t{}\t{}\t{}\t{}\t{}\n'.format(
                    ts, ap, res_channel['active_time'], res_channel['busy_time'],
                    res_sig_rate['signal'], res_sig_rate['signal_avg'], res_sig_rate['tx_rate'])
            self.openfile.write(data_str)
            time.sleep(0.5)
    def stop(self):
        self.cont = False
        time.sleep(1)           
        if(self.openfile is not None):
            self.openfile.flush()
            self.openfile.close()
            self.openfile = None

class HandoffThread(threading.Thread):
    def __init__(self, iface, essid):
        threading.Thread.__init__(self)
        self.essid = essid
        self.apmgr = APManager(iface)
        self.cont = True

    def run(self):
        while(self.cont):
            old_ap = None
            cur_ap_mac = self.apmgr.get_current_ap()
            aps = self.apmgr.get_aps(self.essid)
            for ap in aps:
                print(ap)

            if(len(aps)<1):
                continue

            best_ap = aps[0]
            for ap in aps:
                if(ap.mac == cur_ap_mac):
                    old_ap = ap
                if(ap.signal > best_ap.signal):
                    best_ap = ap

            if(old_ap is None):
                (timing, status) = self.apmgr.associate_to(best_ap)
            elif(old_ap.mac != best_ap.mac and (best_ap.signal - old_ap.signal) > 10):
                self.apmgr.associate_to(best_ap)

        time.sleep(0.5)

    def stop(self):
        self.cont = False
