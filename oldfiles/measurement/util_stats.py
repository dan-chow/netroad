
class StatsInfo():
    def __init__(self, timestamp, ap_mac, chnl_active_time, chnl_busy_time, signal, signal_avg, tx_rate):
        self.timestamp = timestamp
        self.ap_mac = ap_mac
        self.chnl_active_time = chnl_active_time
        self.chnl_busy_time = chnl_busy_time
        self.signal = signal
        self.signal_avg = signal_avg
        self.tx_rate = tx_rate

class StatsProcessor():
    def __init__(self, infile):
        self.stats_infos = []
        self.aps = set()

        with open(infile) as f:
            for line in f:
                line = line.rstrip()
                tokens = line.split('\t')
                self.stats_infos.append(StatsInfo(long(float(tokens[0])), tokens[1], int(tokens[2]), int(tokens[3]),
                    int(tokens[4]), int(tokens[5]), float(tokens[6])))
                self.aps.add(tokens[1])

        self.start_time = self.stats_infos[0].timestamp
        self.end_time = self.stats_infos[len(self.stats_infos) -1].timestamp
        print('stats_time:{}'.format(self.end_time - self.start_time))
    
    def get_start_time(self):
        return self.start_time

    def get_stats(self, interval, start_time=None):
        if start_time is None:
            start_time = self.start_time
        t_start = start_time
        ap_stats = {}
        for ap in self.aps:
            ap_stats[ap] = {}
            ap_stats[ap]['timestamp'] = []
            ap_stats[ap]['utilization'] = []
            ap_stats[ap]['signal'] = []
            ap_stats[ap]['signal_avg'] = []
            ap_stats[ap]['tx_rate'] = []

        utilization = []
        signal = []
        signal_avg = []
        tx_rate = []

        mac_start = self.stats_infos[0].ap_mac
        t_start = self.start_time - start_time
        for stats_info in self.stats_infos:
            if((stats_info.timestamp - start_time - t_start)< interval and stats_info.ap_mac==mac_start):
                utilization.append(1.0 * stats_info.chnl_busy_time / stats_info.chnl_active_time)
                signal.append(stats_info.signal)
                signal_avg.append(stats_info.signal_avg)
                tx_rate.append(stats_info.tx_rate)
            else:
                ap_stats[mac_start]['timestamp'].append(t_start)
                ap_stats[mac_start]['utilization'].append(sum(utilization)/len(utilization))
                ap_stats[mac_start]['signal'].append(sum(signal)/len(signal))
                ap_stats[mac_start]['signal_avg'].append(sum(signal_avg)/len(signal_avg))
                ap_stats[mac_start]['tx_rate'].append(sum(tx_rate)/len(tx_rate))

                mac_start = stats_info.ap_mac
                t_start = stats_info.timestamp - start_time

                utilization = [(1.0 * stats_info.chnl_busy_time / stats_info.chnl_active_time)]
                signal = [(stats_info.signal)]
                signal_avg = [(stats_info.signal_avg)]
                tx_rate = [(stats_info.tx_rate)]

        return ap_stats
