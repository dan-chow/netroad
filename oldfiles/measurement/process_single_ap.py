from util_stats import StatsProcessor
from util_pcap import PcapProcessor
import matplotlib.pyplot as plt
import sys

if __name__ == '__main__':
    test_name = 'walk'
    test_cnt = 1
    res_stats = []
    
    for i in range(test_cnt):
        stats_file = './result_single_ap/walk{}.stats'.format(str(i+1).zfill(2))
        stats_proc = StatsProcessor(stats_file)
        stats_start_time = stats_proc.get_start_time()
        res_stats.append(stats_proc.get_stats(2))

    mac = res_stats[0].keys()[0]

    for i in range(test_cnt):
        plt.plot(res_stats[i][mac]['timestamp'], res_stats[i][mac]['utilization'])
    plt.xlabel('time')
    plt.ylabel('utilization')
    plt.show()

    for i in range(test_cnt):
        plt.plot(res_stats[i][mac]['timestamp'], res_stats[i][mac]['signal'])
        plt.plot(res_stats[i][mac]['timestamp'], res_stats[i][mac]['signal_avg'])
    plt.xlabel('time')
    plt.ylabel('signal level (dBm)')
    plt.show()

    for i in range(test_cnt):
        plt.plot(res_stats[i][mac]['timestamp'], res_stats[i][mac]['tx_rate'])
    plt.xlabel('time')
    plt.ylabel('tx rate (Mbps)')
    plt.show()

    res_pcap = []
    for i in range(test_cnt):
        pcap_file = './result_single_ap/walk{}.pcap'.format(str(i+1).zfill(2))
        pcap_proc = PcapProcessor(pcap_file)
        pcap_start_time = pcap_proc.get_start_time()
        res_pcap.append(pcap_proc.get_stats(2))

    for i in range(test_cnt):
        plt.plot(res_pcap[i]['timestamp'], res_pcap[i]['rx_rate'])
    plt.xlabel('time')
    plt.ylabel('rx rate (Kbps)')
    plt.show()
