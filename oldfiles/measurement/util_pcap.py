from pcapfile import savefile
from pcapfile.protocols.linklayer import ethernet
from pcapfile.protocols.network import ip
from pcapfile.protocols.transport import tcp
import binascii

def get_max_key(in_dir):
    max_key = None
    max_value = -1
    for key in in_dir:
        if(in_dir[key] > max_value):
            max_key = key
            max_value = in_dir[key]
    return max_key

class PacketInfo():
    def __init__(self, timestamp, pkt_size, src_ip, dst_ip, seqnum, acknum):
        self.timestamp = timestamp
        self.pkt_size = pkt_size
        self.src_ip = src_ip
        self.dst_ip = dst_ip
        self.seqnum = seqnum
        self.acknum = acknum

class PcapProcessor():
    def __init__(self, infile):
        self.pkt_infos = []
        self.client_ip = None
        self.server_ip = None

        inpcap = open(infile, 'rb')
        capfile = savefile.load_savefile(inpcap, verbose=False)
        all_pkts = capfile.packets
        inpcap.close()

        self.start_time = all_pkts[0].timestamp
        self.end_time = all_pkts[len(all_pkts)-1].timestamp
        print('pcap_time:{}'.format(self.end_time - self.start_time))

        ip_dir = {}
        for pkt in all_pkts:
            eth_frame = ethernet.Ethernet(pkt.raw())

            if(eth_frame.type != 0x0800):
                continue
            ip_pkt = ip.IP(binascii.unhexlify(eth_frame.payload))
            ip_dir[ip_pkt.src] = 1 if ip_pkt.src not in ip_dir else (ip_dir[ip_pkt.src] + 1)
            ip_dir[ip_pkt.dst] = 1 if ip_pkt.dst not in ip_dir else (ip_dir[ip_pkt.dst] + 1)

            if(ip_pkt.p != 6):
                continue
            tcp_seg = tcp.TCP(binascii.unhexlify(ip_pkt.payload))

            self.pkt_infos.append(PacketInfo(pkt.timestamp, len(pkt.raw()), 
                ip_pkt.src, ip_pkt.dst, tcp_seg.seqnum, tcp_seg.acknum))

        max_key = get_max_key(ip_dir)
        if('192.168.8' in max_key):
            self.client_ip = max_key
        else:
            self.server_ip = max_key
        del ip_dir[max_key]
        
        max_key = get_max_key(ip_dir)
        if(self.client_ip is None):
            self.client_ip = max_key
        else:
            self.server_ip = max_key

        print('server:{}'.format(self.server_ip))
        print('client:{}'.format(self.client_ip))

    def is_tx(self, pkt_info):
        return (pkt_info.src_ip == self.client_ip and pkt_info.dst_ip == self.server_ip)

    def is_rx(self, pkt_info):
        return (pkt_info.src_ip == self.server_ip and pkt_info.dst_ip == self.client_ip)

    def get_start_time(self):
        return self.pkt_infos[0].timestamp
    
    def get_stats(self, interval, start_time=None):
        if start_time is None:
            start_time = self.start_time

        res = {}
        res['timestamp'] = []
        res['tx_rate'] = []
        res['rx_rate'] = []
        res['tx_loss_rate'] = []
        res['rx_loss_rate'] = []

        client_seq = set()
        server_ack = set()

        tx_bytes = 0
        rx_bytes = 0
        tx_pkts = 0
        rx_pkts = 0
        tx_loss = 0
        rx_loss = 0
        
        t_start = self.start_time - start_time

        for pkt_info in self.pkt_infos:
            if((pkt_info.timestamp - start_time - t_start) < interval):
                if(self.is_tx(pkt_info)):
                    tx_bytes += pkt_info.pkt_size
                    tx_pkts += 1

                    if(pkt_info.seqnum not in client_seq):
                        client_seq.add(pkt_info.seqnum)
                    else:
                        tx_loss += 1
                    continue

                if(self.is_rx(pkt_info)):
                    rx_bytes += pkt_info.pkt_size
                    rx_pkts += 1

                    if(pkt_info.acknum not in server_ack):
                        server_ack.add(pkt_info.acknum)
                    else:
                        rx_loss += 1
            else:

                # TODO loss rate may be wrong.
                res['timestamp'].append(t_start)
                res['tx_rate'].append((8.0 * tx_bytes / interval / 1024) if tx_bytes > 0 else 0)
                res['rx_rate'].append((8.0 * rx_bytes / interval / 1024) if rx_bytes > 0 else 0)
                res['tx_loss_rate'].append((1.0 * tx_loss / tx_pkts) if tx_pkts > 0 else 0)
                res['rx_loss_rate'].append((1.0 * rx_loss / rx_pkts) if rx_pkts > 0 else 0)

                client_seq.clear()
                server_ack.clear()

                if(self.is_tx(pkt_info)):
                    tx_bytes = pkt_info.pkt_size
                    tx_pkts = 1
                    client_seq.add(pkt_info.seqnum)
                
                if(self.is_rx(pkt_info)):
                    rx_bytes = pkt_info.pkt_size
                    rx_pkts = 1
                    server_ack.add(pkt_info.acknum)

                tx_loss = 0
                rx_loss = 0

                t_start = pkt_info.timestamp - start_time

        return res
