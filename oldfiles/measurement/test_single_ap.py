#! /usr/bin/env python

from util_threads import WgetThread, TcpdumpThread, StatsThread
import sys
import os
import signal

def cleanup(threads_to_stop):
    for t in threads_to_stop:
        if(t.isAlive()):
            t.stop()

def test_single_ap(iface, testname):
    file_dl = './result_single_ap/{}.dl'.format(testname)
    file_pcap = './result_single_ap/{}.pcap'.format(testname)
    file_stats = './result_single_ap/{}.stats'.format(testname)
    
    if not os.path.exists('./result_single_ap'):
        os.mkdir('./result_single_ap')

    if(os.path.exists(file_dl) or os.path.exists(file_pcap) or os.path.exists(file_stats)):
        print('file exists!')
        exit(2)

    url = 'http://multipath-tcp.org/snapshots/mptcp_2016_04_11.tar.gz'
    t_wget = WgetThread(url, file_dl)
    t_wget.start()

    t_tcpdump = TcpdumpThread(iface, file_pcap)
    t_tcpdump.start()

    t_stats = StatsThread(iface, file_stats)
    t_stats.start()

    while(True):
        cmd = raw_input('press t to terminate:')
        if(cmd.lower() == 't'):
            cleanup([ t_stats, t_tcpdump, t_wget ])
            os._exit(0)

if __name__ == '__main__':
    if(len(sys.argv) <= 2):
        print('Usage: {} iface testname'.format(sys.argv[0]))
        sys.exit(1)

    test_single_ap(sys.argv[1], sys.argv[2])
    # TODO do post processing here.
