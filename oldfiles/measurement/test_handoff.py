#! /usr/bin/env python

from util_ap import APManager
from subprocess import Popen, PIPE
import sys
import os
import time

if __name__ == '__main__':
    if(len(sys.argv) <= 4):
        print('Usage: {} iface mac1 mac2 testnum'.format(sys.argv[0]))
        sys.exit(1)

    iface = sys.argv[1]
    mac1 = sys.argv[2]
    mac2 = sys.argv[3]
    testnum = int(sys.argv[4])

    apmgr = APManager(iface)

    succ = []
    fail = 0

    for i in range(testnum):
        (status, timing) = apmgr.associate_to(mac1) if apmgr.get_current_ap() != mac1 else apmgr.associate_to(mac2)
        if(status == APManager.status_ok):
            print('succeeded')
            succ.append(timing)
        else:
            print('failed')
            fail += 1

        time.sleep(3)

    avg = 1.0 * sum(succ) / len(succ)

    f = open('result.hdf', 'w')
    f.write('avg_handoff_time={}, fail_cnt={}'.format(avg, fail))
    f.flush()
    f.close()
