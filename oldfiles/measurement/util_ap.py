from subprocess import Popen, PIPE
import re
import time

def extract_num(in_str):
    m = re.search(r'[+-]?\d+(\.\d+)*', in_str)
    return None if m is None else m.group()


class APInfo():
    def __init__(self, in_str):
        self.mac = re.search(r'Address.*', in_str).group()
        self.mac = re.search(r'([0-9a-fA-F]{2}:){5}[0-9a-fA-F]{2}', self.mac).group()
        
        self.signal= re.search(r'Signal level.*dBm', in_str).group()
        self.signal = int(extract_num(self.signal))
        
        self.essid = re.search(r'ESSID.*', in_str).group()
        self.essid.replace('ESSID:"', '').replace('"', '')

    def __str__(self):
        return 'essid:' + self.essid + ', mac:' + self.mac + ', sig:' + str(self.signal)

class APManager():
    status_ok = 0
    status_fail = 1
    def __init__(self, iface):
        self.iface = iface
    
    def get_aps(self, essid=None):
        cmd = 'iwlist {} scanning'.format(self.iface)
        proc = Popen(cmd.split(), shell=False, stdout=PIPE, stderr=PIPE)
        (out, err) = proc.communicate()
        
        result = out.split('Cell')[1:]
        if(essid is not None):
            result = [r for r in result if '"{}"'.format(essid) in r]
        return [APInfo(r) for r in result]

    def associate_to(self, target_mac):
        t_start = time.time()

        print('associating to {}...'.format(target_mac))
        
        cmd = 'wpa_cli -i {} bssid 0 {}'.format(self.iface, target_mac)
        print(cmd)
        proc = Popen(cmd.split(), shell=False, stdout=PIPE, stderr=PIPE)
        (out, err) = proc.communicate()
        print(out)
        
        cmd = 'wpa_cli -i {} enable_network 0'.format(self.iface)
        print(cmd)
        proc = Popen(cmd.split(), shell=False, stdout=PIPE, stderr=PIPE)
        (out, err) = proc.communicate()
        print(out)
        
        cmd = 'wpa_cli -i {} reassociate'.format(self.iface)
        print(cmd)
        proc = Popen(cmd.split(), shell=False, stdout=PIPE, stderr=PIPE)
        (out, err) = proc.communicate()
        print(out)
        
        while( target_mac != self.get_current_ap()):
            if(time.time() - t_start > 20):
                return (APManager.status_fail, time.time()-t_start)
            
        return (APManager.status_ok, (time.time()-t_start))
        

    def get_current_ap(self):
        cmd = 'iwconfig {}'.format(self.iface)
        proc = Popen(cmd.split(), shell=False, stdout=PIPE, stderr=PIPE)
        (out, err) = proc.communicate()
        
        if('Not-Associated' in out):
            return None
        
        ap = re.search(r'Access Point:.*', out).group()
        return re.search(r'([0-9a-fA-F]{2}:){5}[0-9a-fA-F]{2}', ap).group()

    def get_channel_stats(self):
         cmd = 'iw dev {} survey dump'.format(self.iface)
         proc = Popen(cmd.split(), shell=False, stdout=PIPE, stderr=PIPE)
         (out, err) = proc.communicate()
         
         res = {}
         res['active_time'] = re.search(r'active time:.*ms', out).group()
         res['active_time'] = extract_num(res['active_time'])       
         res['busy_time'] = re.search(r'busy time:.*ms', out).group()
         res['busy_time'] = extract_num(res['busy_time'])

         return res
         
    def get_signal_and_rate_stats(self):
        cmd = 'iw dev {} station dump'.format(self.iface)
        proc = Popen(cmd.split(), shell=False, stdout=PIPE, stderr=PIPE)
        (out, error) = proc.communicate()                           

        res = {}
        res['signal'] = re.search(r'signal:.*dBm', out).group()
        res['signal'] = extract_num(res['signal'])
        res['signal_avg'] = re.search(r'signal avg:.*dBm', out).group()
        res['signal_avg'] = extract_num(res['signal_avg'])
        res['tx_rate'] = re.search(r'tx bitrate:.*MBit/s', out).group()
        res['tx_rate'] = extract_num(res['tx_rate'])

        return res
