#! /bin/ash

# this file parse dumped survey data to json
# ash differs from bash (a little bit)

contains(){
	string=$1
	substring=$2
	
	if test "${string#*$substring}" != "$string"; then
        	echo true
	else
	        echo false
	fi
}

if [ $# -ne 1 ]; then
	echo "usage: $0 filename"
	exit 1
fi

result="["

is_first_entry=true

while read line; do

	# Survey data from wlan0
	if [ $(contains "$line" "Survey") = true ]; then
		if [ $is_first_entry = true ]; then
			result="$result{"
			is_first_entry=false
		else
			result="$result},{"
		fi
		continue
	fi

	# frequency:			2412 MHz [in use]
	if [ $(contains "$line" "frequency") = true ]; then
		freq=$(echo $line | egrep -o '[0-9]+.*MHz' | egrep -o '[0-9]+')
		result="$result\"freq\":$freq"
		continue
	fi

	# noise:				-93 dBm
	if [ $(contains "$line" "noise") = true ]; then
		noise=$(echo $line | egrep -o '[+-]?[0-9]+.*dBm' | egrep -o '[+-]?[0-9]+')
		result="$result,\"noise\":$noise"
		continue
	fi

	# channel active time:		6924994 ms
	if [ $(contains "$line" "channel active time") = true ]; then
		active_time=$(echo $line | egrep -o '[0-9]+.*ms' | egrep -o '[0-9]+')
		result="$result,\"active_time\":$active_time"
		continue
    fi

	# channel busy time:		3353389 ms
	if [ $(contains "$line" "channel busy time") = true ]; then
		busy_time=$(echo $line | egrep -o '[0-9]+.*ms' | egrep -o '[0-9]+')
		result="$result,\"busy_time\":$busy_time"
		continue
	fi

	# channel receive time:		2287155 ms
	if [ $(contains "$line" "channel receive time") = true ]; then
		receive_time=$(echo $line | egrep -o '[0-9]+.*ms' | egrep -o '[0-9]+')
		result="$result,\"rcv_time\":$receive_time"
		continue
	fi

	# channel transmit time:		106205 ms
	if [ $(contains "$line" "channel transmit time") = true ]; then
		trans_time=$(echo $line | egrep -o '[0-9]+.*ms' | egrep -o '[0-9]+')
		result="$result,\"trans_time\":$trans_time"
		continue
	fi

done < $1

result="$result}]"

echo $result
