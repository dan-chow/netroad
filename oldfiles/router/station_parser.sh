#! /bin/ash

# input:	data dumped by iw dev wlan0 station dump
# output:	json format
# ash differs from bash (a little bit)

contains(){
	string=$1
	substring=$2
	
	if test "${string#*$substring}" != "$string"; then
        	echo true
	else
	        echo false
	fi
}

if [ $# -ne 1 ]; then
	echo "usage: $0 filename"
	exit 1
fi

result="["

is_first_entry=true

while read line; do

	# Station 1c:4b:d6:88:e1:02 (on wlan0)
	if [ $(contains "$line" "Station") = true ]; then
		if [ $is_first_entry = true ]; then
			result="$result{"
			is_first_entry=false
		else
			result="$result},{"
		fi
		mac=$(echo $line | egrep -o '([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})')
		result="$result\"mac\":\"$mac\""
		continue
	fi

	# tx packets:	269
	if [ $(contains "$line" "tx packets") = true ]; then
		tx_pkts=$(echo $line | egrep -o '[0-9]+')
		result="$result,\"tx_pkts\":$tx_pkts"
		continue
	fi

	# tx retries:	19
	if [ $(contains "$line" "tx retries") = true ]; then
		tx_retries=$(echo $line | egrep -o '[0-9]+')
		result="$result,\"tx_retries\":$tx_retries"
		continue
	fi

	# tx failed:	0
	if [ $(contains "$line" "tx failed") = true ]; then
		tx_failed=$(echo $line | egrep -o '[0-9]+')
		result="$result,\"tx_failed\":$tx_failed"
		continue
	fi

	# tx bitrate:	52.0 MBit/s MCS 5
	if [ $(contains "$line" "tx bitrate") = true ]; then
		tx_rate=$(echo $line | egrep -o '[0-9]+.*MBit/s' | egrep -o '[0-9]+(\.[0-9]+)*')
		result="$result,\"tx_rate\":$tx_rate"
		continue
	fi

	# rx bitrate:	65.0 MBit/s MCS 7
	if [ $(contains "$line" "rx bitrate") = true ]; then
		rx_rate=$(echo $line | egrep -o '[0-9]+.*MBit/s' | egrep -o '[0-9]+(\.[0-9]+)*')
		result="$result,\"rx_rate\":$rx_rate"
		continue
	fi

	# signal:  	-19 [-33, -19] dBm
	if [ $(contains "$line" "signal:") = true ]; then
		signal=$(echo $line | egrep -o 'signal.*\[' | egrep -o '[+-]?[0-9]+')
		result="$result,\"signal\":$signal"
		continue
	fi

	# signal avg:	-7 [-9, -22] dBm
	if [ $(contains "$line" "signal avg:") = true ]; then
		signal_avg=$(echo $line | egrep -o 'signal.*\[' | egrep -o '[+-]?[0-9]+')
		result="$result,\"signal_avg\":$signal_avg"
		continue
	fi

done < $1

result="$result}]"

echo $result
