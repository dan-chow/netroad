#! /usr/bin/env python

from subprocess import Popen, PIPE
import re
import sys


def get_throughput(server_ip):
	cmd = "iperf -c %s -t 30 | grep -E 'bits/sec'" % server_ip

	proc = Popen(cmd, stdout=PIPE, shell=True)
	(out, err) = proc.communicate()

	throughput_match = re.search(r'\d+.\d+ Mbits/sec', out)
	if throughput_match is None:
		throughput_match = re.search(r'\d+.\d+ Kbits/sec', out)

	throughput_str = throughput_match.group()

	return throughput_str


if __name__ == "__main__":

	if len(sys.argv) < 2:
		print
		"Usage: %s server_ip" % sys.argv[0]
		sys.exit(-1)

	server_ip = sys.argv[1]

	print
	'throughput=%s' % get_throughput(server_ip)
