#! /bin/bash

cmd_result=$(iw dev wlan0 survey dump)

is_first_entry=true

printf "["
for line in $cmd_result
do
    if [[ $line == *"Survey"* ]]; then
	    if [ $is_first_entry = true ]; then
		    printf "{"
		    is_first_entry=false
	    else
		    printf "},{"
        fi
    fi

    if [[ $line == *"frequency"* ]]; then
        frequency=$(echo $line | egrep -o '[0-9]+.*MHz' | egrep -o '[0-9]+')
    	printf "\"frequency\":%d" $frequency
    fi

    if [[ $line == *"noise"* ]]; then
        noise=$(echo $line | egrep -o '[0-9]+.*dBm' | egrep -o '[0-9]+')
        printf ",\"noise\":%d" $signal
    fi

    if [[ $line == *"channel active time"* ]]; then
        active_time=$(echo $line | egrep -o '[0-9]+.*ms' | egrep -o '[0-9]+')
        printf ",\"channel active time\":%d" $active_time
    fi

    if [[ $line == *"channel busy time"* ]]; then
        busy_time=$(echo $line | egrep -o '[0-9]+.*ms' | egrep -o '[0-9]+')
        printf ",\"channel busy time\":%d" $busy_time
    fi

    if [[ $line == *"channel receive time"* ]]; then
        receive_time=$(echo $line | egrep -o '[0-9]+.*ms' | egrep -o '[0-9]+')
        printf ",\"channel receive time\":%d" $receive_time
    fi

    if [[ $line == *"channel transmit time"* ]]; then
        transmit_time=$(echo $line | egrep -o '[0-9]+.*ms' | egrep -o '[0-9]+')
        printf ",\"channel transmit time\":%d" $transmit_time
    fi

done

printf "}]"
