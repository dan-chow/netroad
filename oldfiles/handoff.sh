#! /bin/bash

if [[ $# != 2 ]]; then
    echo "Usage: $0 iface dst_mac"
    exit 1
fi

iface=$1
dst_mac=$2

old_ap_mac=$(iwconfig $1 | grep -Eo '([0-9a-fA-F]{2}:){5}[0-9a-fA-F]{2}')
echo "old ap mac: $old_ap_mac"

sudo wpa_cli -i $iface bssid 0 $dst_mac
sudo wpa_cli -i $iface enable_network 0
sudo wpa_cli -i $iface reassociate

new_ap_mac=$(iwconfig $1 | grep -Eo '([0-9a-fA-F]{2}:){5}[0-9a-fA-F]{2}')
echo "new ap mac: $new_ap_mac"


