#! /bin/bash

load=$1
throughput=$(python get_throughput.py 114.212.85.193)
channel_utilization=$(python get_channel_utilization.py wlan3)
delay=$(python get_delay.py 114.212.85.193)
beacon=$(sudo python get_beacon_info.py wlan3 C0:61:18:FC:8B:2C)

echo load=$load,$throughput,$channel_utilization,$delay,$beacon
