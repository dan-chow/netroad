#! /usr/bin/env python

from subprocess import Popen, PIPE
import re
import sys
import numpy as np


def get_delay(server_ip):
	cmd = "ping %s -c 1" % server_ip
	proc = Popen(cmd, stdout=PIPE, shell=True)
	(out, err) = proc.communicate()

	cmd = "ping %s -c 30" % server_ip
	proc = Popen(cmd, stdout=PIPE, shell=True)
	(out, err) = proc.communicate()

	times = re.findall(r'time=\d+.\d+', out)
	times = [float(re.search(r'\d+.\d+', t).group()) for t in times]

	return np.mean(times)


if __name__ == "__main__":

	if len(sys.argv) < 2:
		print
		"Usage: %s server_ip" % sys.argv[0]
		sys.exit(-1)

	server_ip = sys.argv[1]

	print
	'delay=%s' % get_delay(server_ip)
