import matplotlib.pyplot as plt
import re
import os


def plot_and_save(x_data, y_data, x_label, y_label, title, filename):
	plt.figure()
	plt.plot(x_data, y_data, 'bo')
	plt.suptitle(title)
	plt.xlabel(x_label)
	plt.ylabel(y_label)
	plt.savefig(filename)
	plt.close()


load = []
throughput = []
channel_utilization = []
delay = []
beacon_number = []
beacon_interval = []

with open('load_result_0115.txt') as f:
	for line in f:
		tokens = [s[s.index('=') + 1:] for s in line.rstrip().split(',')]

		load.append(float(tokens[0]))
		throughput_str = re.search(r'\d+.\d+', tokens[1]).group()
		throughput.append(float(throughput_str))
		channel_utilization.append(float(tokens[2]))
		delay.append(float(tokens[3]))
		beacon_number.append(int(tokens[4]))
		beacon_interval.append(float(tokens[5]))

if not os.path.exists('./pic'):
	os.mkdir('./pic')

plot_and_save(
	x_data=load,
	y_data=throughput,
	x_label='load',
	y_label='throughput',
	title='throughput vs. load',
	filename='./pic/throughput_vs_load.png')

plot_and_save(
	x_data=load,
	y_data=channel_utilization,
	x_label='load',
	y_label='channel utilization',
	title='channel utilization vs. load',
	filename='./pic/channel_utilization_vs_load.png')

plot_and_save(
	x_data=load,
	y_data=delay,
	x_label='load',
	y_label='delay',
	title='delay vs. load',
	filename='./pic/delay_vs_load.png')

plot_and_save(
	x_data=load,
	y_data=beacon_number,
	x_label='load',
	y_label='beacon number',
	title='beacon number vs. load',
	filename='./pic/beacon_number_vs_load.png')

plot_and_save(
	x_data=load,
	y_data=beacon_interval,
	x_label='load',
	y_label='beacon interval',
	title='beacon interval vs. load',
	filename='./pic/beacon_interval_vs_load.png')

# throughput = []
# channel_utilization = []
# delay = []
# beacon_number = []
# beacon_interval = []

plot_and_save(
	x_data=channel_utilization,
	y_data=throughput,
	x_label='channel utilization',
	y_label='throughput',
	title='throughput vs. channel utilization',
	filename='./pic/throughput_vs_channel_utilization.png')

plot_and_save(
	x_data=delay,
	y_data=throughput,
	x_label='delay',
	y_label='throughput',
	title='throughput vs. delay',
	filename='./pic/throughput_vs_delay.png')

plot_and_save(
	x_data=beacon_number,
	y_data=throughput,
	x_label='beacon_number',
	y_label='throughput',
	title='throughput vs. beacon number',
	filename='./pic/throughput_vs_beacon_number.png')

plot_and_save(
	x_data=beacon_interval,
	y_data=throughput,
	x_label='beacon interval',
	y_label='throughput',
	title='throughput vs. beacon interval',
	filename='./pic/throughput_vs_beacon_interval.png')
